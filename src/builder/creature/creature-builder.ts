import { ActorBuilder } from '../common/actor-builder'
import WaiterUtil from '../../util/waiter-util'
import { CreatureModel } from '../../models/creature/creature-model'
import { CreatureModelBuilder } from '../../models/creature/creature-model-builder'
import FolderUtil from '../../util/folder-util'
import EntityUtil from '../../util/entity-util'
import TrappingUtil from '../../util/trapping-util'
import { GenerateEffectOptionEnum } from '../../util/generate-effect-option.enum'
import RandomUtil from '../../util/random-util'
import ReferentialUtil from '../../util/referential-util'
import StringUtil from '../../util/string-util'
import CompendiumUtil from '../../util/compendium-util'
import { FoundryActor, FoundryPrototypeToken } from '../../types/foundry/actor'
import { FoundryItem } from '../../types/foundry/item'
import { FoundryCareerData } from '../../types/foundry/career'

export class CreatureBuilder extends ActorBuilder {
  public static async buildCreature(
    model: CreatureModel,
    callback: (model: CreatureModelBuilder) => void,
  ) {
    const modelBuilder = model.fromBuilder ?? new CreatureModelBuilder(model)
    await WaiterUtil.show(
      'WFRP4NPCGEN.creature.generation.inprogress.title',
      'WFRP4NPCGEN.creature.generation.inprogress.hint',
      async () => {
        console.info('Prepare Basic skills')
        await this.addBasicSkill(modelBuilder)

        console.info('Prepare Basic Chars')
        await this.addBasicChars(modelBuilder)

        console.info('Prepare Swarm')
        await this.addSwarm(modelBuilder)

        console.info('Prepare Size')
        await this.addSize(modelBuilder)

        console.info('Prepare Weapon')
        await this.addWeapon(modelBuilder)

        console.info('Prepare Ranged')
        await this.addRanged(modelBuilder)

        console.info('Prepare Armour')
        await this.addArmour(modelBuilder)

        if (model.generateAsPnj) {
          console.info('Prepare Career Path')
          await this.addCareerPath(modelBuilder)

          console.info('Prepare Status')
          await this.addStatus(modelBuilder)

          console.info('Prepare Native Tongue')
          await this.addNativeTongueSkill(modelBuilder)

          console.info('Prepare City Born')
          await this.addCityBornSkill(modelBuilder)

          console.info('Prepare Career Skills')
          await this.addCareerSkill(modelBuilder)

          console.info('Prepare Career Talents')
          await this.addCareerTalents(modelBuilder)

          console.info('Prepare Skills Advances')
          await this.addAdvanceSkills(modelBuilder)

          console.info('Prepare Chars Advances')
          await this.addAdvanceChars(modelBuilder)

          const trappings: string[] = []
          if (model.options.withClassTrappings) {
            console.info('Prepare Class Trappings')
            await this.prepareClassTrappings(modelBuilder, trappings)
          }
          if (model.options.withCareerTrappings) {
            console.info('Prepare Career Trappings')
            await this.prepareCareerTrappings(modelBuilder, trappings)
          }
          console.info('Prepare Trappings')
          await this.addTrappings(modelBuilder, trappings)

          await this.editAbilities(modelBuilder, callback)
        } else {
          await this.editTrappings(modelBuilder, callback)
        }
      },
    )
  }

  public static async buildCreatureData(model: CreatureModelBuilder) {
    const actorData: FoundryActor = {
      name: model.model.name,
      type: model.model.generateAsPnj ? 'npc' : 'creature',
      flags: {
        autoCalcRun: true,
        autoCalcWalk: true,
        autoCalcWounds: true,
        autoCalcCritW: true,
        autoCalcCorruption: true,
        autoCalcEnc: true,
        autoCalcSize: true,
      },
      system: {
        characteristics: model.chars,
        details: {
          move: {
            value: model.move,
          },
          size: {
            value: model.sizeKey,
          },
          species: {
            value: model.speciesValue,
          },
          biography: {
            value: model.biography,
          },
          gender: {
            value: model.gender,
          },
          hitLocationTable: {
            value: model.hitLocationTable,
          },
          status: {
            value: model.status,
          },
        },
      },
      items: model.model.generateAsPnj
        ? [
            ...model.careers.map((c) => {
              const cd = duplicate(c)
              delete cd._id
              return cd
            }),
          ]
        : [{ name: 'Fake Item', type: 'skill' } as FoundryItem], // needed to prevent add basic skill dialog
    }
    if (
      model.model.options?.imagePath != null &&
      model.model.options.imagePath.length > 0
    ) {
      actorData.img = model.model.options.imagePath
    }
    return Promise.resolve(actorData)
  }

  public static async createCreature(
    model: CreatureModelBuilder,
    data: FoundryActor,
  ) {
    const options = model?.model?.options
    if (options?.genPath?.length > 0 || options?.withGenPathCareerName) {
      const genPaths =
        options?.genPath?.length > 0
          ? options?.genPath.split('/').filter((p) => p != null && p.length > 0)
          : []
      if (model.model.generateAsPnj && options?.withGenPathCareerName) {
        const careerData: FoundryCareerData = model.career?.system
        genPaths.push(careerData?.careergroup?.value as string)
      }
      if (genPaths.length > 0) {
        const folder = await FolderUtil.createNamedFolder(genPaths.join('/'))
        data.folder = folder?.id
      }
    }

    let actor: FoundryActor = await Actor.create(data)
    if (!model.model.generateAsPnj) {
      const fakeSkill = actor.itemTypes?.skill?.find(
        (s) => s.name === 'Fake Item',
      )
      if (fakeSkill != null && actor.deleteEmbeddedDocuments) {
        await actor.deleteEmbeddedDocuments(Item.metadata.name, [fakeSkill.id])
      }
    }
    if (model.skills.length > 0 && actor.createEmbeddedDocuments) {
      await actor.createEmbeddedDocuments(
        Item.metadata.name,
        EntityUtil.toDuplicateRecords(model.skills),
        { skipSpecialisationChoice: true },
      )
    }
    if (model.talents.length > 0 && actor.createEmbeddedDocuments) {
      await actor.createEmbeddedDocuments(
        Item.metadata.name,
        EntityUtil.toDuplicateRecords(model.talents),
          { skipSpecialisationChoice: true },
      )
    }
    if (model.traits.length > 0 && actor.createEmbeddedDocuments) {
      await actor.createEmbeddedDocuments(
        Item.metadata.name,
        EntityUtil.toRecords(model.traits),
          { skipSpecialisationChoice: true },
      )
    }
    if (model.trappings.length > 0 && actor.createEmbeddedDocuments) {
      await actor.createEmbeddedDocuments(
        Item.metadata.name,
        EntityUtil.toDuplicateRecords(model.trappings),
      )
    }
    if (model.spells.length > 0 && actor.createEmbeddedDocuments) {
      await actor.createEmbeddedDocuments(
        Item.metadata.name,
        EntityUtil.toDuplicateRecords(model.spells),
      )
    }
    if (model.prayers.length > 0 && actor.createEmbeddedDocuments) {
      await actor.createEmbeddedDocuments(
        Item.metadata.name,
        EntityUtil.toDuplicateRecords(model.prayers),
      )
    }
    if (model.physicalMutations.length > 0 && actor.createEmbeddedDocuments) {
      await actor.createEmbeddedDocuments(
        Item.metadata.name,
        EntityUtil.toDuplicateRecords(model.physicalMutations),
      )
    }
    if (model.mentalMutations.length > 0 && actor.createEmbeddedDocuments) {
      await actor.createEmbeddedDocuments(
        Item.metadata.name,
        EntityUtil.toDuplicateRecords(model.mentalMutations),
      )
    }
    if (model.others.length > 0 && actor.createEmbeddedDocuments) {
      await actor.createEmbeddedDocuments(
        Item.metadata.name,
        EntityUtil.toDuplicateRecords(model.others),
      )
    }

    if (model.model.generateAsPnj && actor.updateEmbeddedDocuments) {
      for (const career of actor?.itemTypes?.career ?? []) {
        await actor.updateEmbeddedDocuments(Item.metadata.name, [
          {
            _id: career.id,
            'system.complete.value': true,
          },
        ])
      }
    }

    await TrappingUtil.generateMoney(actor)
    await TrappingUtil.initMoney(actor)

    if (options?.withInitialMoney) {
      await TrappingUtil.generateMoney(actor)
    }

    if (model.model.generateAsPnj && options?.withInitialWeapons) {
      await TrappingUtil.generateWeapons(actor)
    }

    if (
      !options?.withLinkedToken &&
      !options?.withInitialMoney &&
      GenerateEffectOptionEnum.NONE !== options.generateMoneyEffect
    ) {
      await this.addGenerateTokenEffect(
        actor,
        'WFRP4NPCGEN.trappings.money.label',
        GenerateEffectOptionEnum.DEFAULT_DISABLED ===
          options.generateMoneyEffect,
        'modules/wfrp4e-core/art/other/gold.webp',
      )
    }

    if (
      model.model.generateAsPnj &&
      !options?.withLinkedToken &&
      !options?.withInitialWeapons &&
      GenerateEffectOptionEnum.NONE !== options.generateWeaponEffect
    ) {
      await this.addGenerateTokenEffect(
        actor,
        'WFRP4NPCGEN.trappings.weapon.label',
        GenerateEffectOptionEnum.DEFAULT_DISABLED ===
          options.generateWeaponEffect,
        'modules/wfrp4e-core/art/other/weapons.webp',
      )
    }

    const token = actor.prototypeToken
    const update: FoundryPrototypeToken = {}
    let performUpdate = false

    if (
      token != null &&
      options?.tokenPath != null &&
      options.tokenPath.length > 0
    ) {
      performUpdate = true
      update.texture = {
        src: options.tokenPath,
      }
      update.randomImg = options.tokenPath.includes('*')
    }

    if (token != null && options?.withLinkedToken) {
      performUpdate = true
      update.actorLink = options?.withLinkedToken
    }

    if (performUpdate && actor.update) {
      actor = await actor.update({
        prototypeToken: mergeObject(token, update, { inplace: false }),
      })
    }

    return Promise.resolve(actor)
  }

  private static async addBasicSkill(model: CreatureModelBuilder) {
    if (model.model.abilities.includeBasicSkills || model.model.generateAsPnj) {
      const skills = await ReferentialUtil.getAllBasicSkills()
      for (let skill of skills) {
        const existingSkill = model.skills.find((s) => s.name === skill.name)
        if (existingSkill == null) {
          model.skills.push(skill)
        }
      }
    }
  }

  private static async addBasicChars(model: CreatureModelBuilder) {
    Object.entries(model.model.creature.system.characteristics).forEach(
      ([key, char]) => {
        let initial = char.value as number
        if (initial > 0) {
          const positive = RandomUtil.getRandomBoolean()
          const amplitude = RandomUtil.getRandomPositiveNumber(6)
          const adjust = model.model?.options?.noRandomCaras
            ? 0
            : (positive ? 1 : -1) *
              RandomUtil.getRandomPositiveNumber(amplitude)
          initial = Math.max(1, initial + adjust)
        }

        model.chars[key] = {
          initial: initial,
          advances: 0,
        }
      },
    )
    if (model.model.template.isSwarm && !model.model.abilities.isSwarm) {
      model.chars.ws.initial -= 20
    } else if (
      (!model.model.abilities.isSwarm && model.model.template.swarm != null) ||
      model.model.template.isSwarm
    ) {
      model.chars.ws.initial -= 10
    }

    const fromSize = ReferentialUtil.sortedSize.indexOf(
      model.model.template.size,
    )
    const toSize = ReferentialUtil.sortedSize.indexOf(model.sizeKey)

    const sizeRatio = Math.abs(toSize - fromSize)
    const smallToBig = toSize > fromSize

    if (sizeRatio > 0) {
      model.chars.s.initial += sizeRatio * 10 * (smallToBig ? 1 : -1)
      model.chars.t.initial += sizeRatio * 10 * (smallToBig ? 1 : -1)
      model.chars.ag.initial += sizeRatio * 5 * (smallToBig ? -1 : 1)
    }

    Object.entries(model.chars).forEach(([_key, char]) => {
      if (char.initial < 0) {
        char.initial = 0
      }
    })
  }

  private static async addSwarm(model: CreatureModelBuilder) {
    const swarm = duplicate(await CompendiumUtil.getCompendiumSwarmTrait())
    if (model.model.abilities.isSwarm) {
      swarm.system.disabled = false
      model.traits.push(swarm)
    } else if (model.model.template.swarm != null) {
      swarm.system.disabled = true
      model.traits.push(swarm)
    }
  }

  private static async addSize(model: CreatureModelBuilder) {
    const size = duplicate(await CompendiumUtil.getCompendiumSizeTrait())
    size.system.specification.value = CompendiumUtil.getSizes()[model.sizeKey]
    size.system.disabled = false
    model.traits.push(size)
  }

  private static async addWeapon(model: CreatureModelBuilder) {
    const weapon = duplicate(await CompendiumUtil.getCompendiumWeaponTrait())
    if (model.model.abilities.hasWeaponTrait) {
      weapon.system.specification.value = Number.isNumeric(
        model.model.abilities.weaponDamage,
      )
        ? Number(model.model.abilities.weaponDamage)
        : 0
      weapon.system.disabled = false
      model.traits.push(weapon)
    } else if (model.model.template.weapon != null) {
      weapon.system.specification.value = Number.isNumeric(
        model.model.abilities.weaponDamage,
      )
        ? Number(model.model.abilities.weaponDamage)
        : Number.isNumeric(model.model.template.weaponDamage)
          ? Number(model.model.template.weaponDamage)
          : 0
      weapon.system.disabled = true
      model.traits.push(weapon)
    }
  }

  private static async addRanged(model: CreatureModelBuilder) {
    const ranged = duplicate(await CompendiumUtil.getCompendiumRangedTrait())
    const defaultRange = StringUtil.getGroupName(ranged.name)
    const defaultDamage = ranged.system.specification.value
    if (model.model.abilities.hasRangedTrait) {
      const range = Number.isNumeric(model.model.abilities.rangedRange)
        ? model.model.abilities.rangedRange
        : defaultRange
      const damage = Number.isNumeric(model.model.abilities.rangedDamage)
        ? model.model.abilities.rangedDamage
        : defaultDamage
      ranged.name = `${StringUtil.getSimpleName(ranged.name).trim()} (${range})`
      ranged.system.specification.value = damage
      ranged.system.disabled = false
      model.traits.push(ranged)
    } else if (model.model.template.ranged != null) {
      const range = Number.isNumeric(model.model.abilities.rangedRange)
        ? model.model.abilities.rangedRange
        : Number.isNumeric(model.model.template.rangedRange)
          ? model.model.template.rangedRange
          : defaultRange
      const damage = Number.isNumeric(model.model.abilities.rangedDamage)
        ? model.model.abilities.rangedDamage
        : Number.isNumeric(model.model.template.rangedDamage)
          ? model.model.template.rangedDamage
          : defaultDamage
      ranged.name = `${StringUtil.getSimpleName(ranged.name).trim()} (${range})`
      ranged.system.specification.value = damage
      ranged.system.disabled = true
      model.traits.push(ranged)
    }
  }

  private static async addArmour(model: CreatureModelBuilder) {
    const armour = duplicate(await CompendiumUtil.getCompendiumArmourTrait())
    if (model.model.abilities.hasArmourTrait) {
      armour.system.specification.value = Number.isNumeric(
        model.model.abilities.armourValue,
      )
        ? Number(model.model.abilities.armourValue)
        : 1
      armour.system.disabled = false
      model.traits.push(armour)
    } else if (model.model.template.armour != null) {
      armour.system.specification.value = Number.isNumeric(
        model.model.abilities.armourValue,
      )
        ? Number(model.model.abilities.armourValue)
        : Number.isNumeric(model.model.template.armourValue)
          ? Number(model.model.template.armourValue)
          : 0
      armour.system.disabled = true
      model.traits.push(armour)
    }
  }

  private static async addNativeTongueSkill(model: CreatureModelBuilder) {
    const nativeTongue = model.model.nativeTongue ?? ''
    if (nativeTongue?.length > 0) {
      await this.addSkill(model, `Language (${nativeTongue})`)
    }
  }
}
