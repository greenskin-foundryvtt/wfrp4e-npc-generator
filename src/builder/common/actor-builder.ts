import CompendiumUtil from '../../util/compendium-util'
import WaiterUtil from '../../util/waiter-util'
import { ActorModelBuilder } from '../../models/common/actor-model-builder'
import { ActorModel } from '../../models/common/actor-model'
import { AbilitiesChooser } from '../../components/abilities-chooser'
import { TrappingChooser } from '../../components/trapping-chooser'
import { MagicChooser } from '../../components/magic-chooser'
import { MutationChooser } from '../../components/mutation-chooser'
import { i18n } from '../../constant'
import ReferentialUtil from '../../util/referential-util'
import StringUtil from '../../util/string-util'
import RandomUtil from '../../util/random-util'
import { FoundryActor } from '../../types/foundry/actor'
import { ActiveEffect } from '../../types/foundry/constant'
import { FoundryActiveEffectCreateData } from '../../types/foundry/active-effect'
import { FoundryCareer, FoundryCareerData } from '../../types/foundry/career'
import NotificationUtil from '../../util/notification-util'

export abstract class ActorBuilder {
  protected static async addGenerateTokenEffect(
    actor: FoundryActor,
    label: string,
    disabled: boolean,
    icon?: string,
  ) {
    const generateEffect: FoundryActiveEffectCreateData = {
      icon: icon,
      label: i18n().localize(label),
      disabled: disabled,
    }
    generateEffect['flags.wfrp4e.effectApplication'] = 'actor'
    if (actor.createEmbeddedDocuments) {
      await actor.createEmbeddedDocuments(ActiveEffect.metadata.name, [
        generateEffect,
      ])
    }
  }

  protected static async editAbilities<T extends ActorModel>(
    model: ActorModelBuilder<T>,
    callback: (model: ActorModelBuilder<T>) => void,
  ) {
    if (model.model.options.editAbilities) {
      try {
        NotificationUtil.silent = true
        await CompendiumUtil.getCompendiumActors()
      } finally {
        NotificationUtil.silent = false
      }
      await WaiterUtil.hide(false)
      await AbilitiesChooser.editAbilities(
        model.skills,
        model.talents,
        model.traits,
        async (skills, talents, traits) => {
          model.skills = skills
          model.talents = talents
          model.traits = traits
          await this.editTrappings(model, callback)
        },
      )
    } else {
      await this.editTrappings(model, callback)
    }
  }

  protected static async editTrappings<T extends ActorModel>(
    model: ActorModelBuilder<T>,
    callback: (model: ActorModelBuilder<T>) => void,
  ) {
    if (model.model.options.editTrappings) {
      const undo = model.model.options.editAbilities
        ? () => {
            this.editAbilities(model, callback)
          }
        : undefined
      await WaiterUtil.hide(false)
      await TrappingChooser.editTrappings(
        model.trappings,
        async (trappings) => {
          model.trappings = trappings
          await this.editMagics(model, callback)
        },
        undo,
      )
    } else {
      await this.editMagics(model, callback)
    }
  }

  protected static async editMagics<T extends ActorModel>(
    model: ActorModelBuilder<T>,
    callback: (model: ActorModelBuilder<T>) => void,
  ) {
    if (model.model.options.addMagics) {
      const undo = model.model.options.editTrappings
        ? () => {
            this.editTrappings(model, callback)
          }
        : undefined
      await WaiterUtil.hide(false)
      await MagicChooser.editMagics(
        model.spells,
        model.prayers,
        async (spells, prayers) => {
          model.spells = spells
          model.prayers = prayers
          await this.editMutations(model, callback)
        },
        undo,
      )
    } else {
      await this.editMutations(model, callback)
    }
  }

  protected static async editMutations<T extends ActorModel>(
    model: ActorModelBuilder<T>,
    callback: (model: ActorModelBuilder<T>) => void,
  ) {
    if (model.model.options.addMutations) {
      const undo = model.model.options.addMagics
        ? () => {
            this.editMagics(model, callback)
          }
        : model.model.options.editTrappings
          ? () => {
              this.editTrappings(model, callback)
            }
          : undefined
      await WaiterUtil.hide(false)
      await MutationChooser.editMutations(
        model.physicalMutations,
        model.mentalMutations,
        (physicals, mentals) => {
          model.physicalMutations = physicals
          model.mentalMutations = mentals
          callback(model)
        },
        undo,
      )
    } else {
      callback(model)
    }
  }

  protected static async addCareerPath<T extends ActorModel>(
    model: ActorModelBuilder<T>,
  ) {
    const careers: FoundryCareer[] = await ReferentialUtil.getCareerEntities()
    let career: FoundryCareer
    const lastCareer = careers.find(
      (c) => c.name === model.model.careers[model.model.careers.length - 1],
    )
    career = lastCareer ?? <FoundryCareer>{}

    model.career = career

    const careerData: FoundryCareerData = career?.system
    if (model.model.careers.length > 1) {
      model.careers = []
      for (let customCareerPath of model.model.careers) {
        const customPath = careers.find((c) => c.name === customCareerPath)
        model.careers.push(customPath ?? <FoundryCareer>{})
      }
    } else if (careerData?.careergroup?.value != null) {
      model.careers = careers
        .filter((c: FoundryCareer) => {
          const data: FoundryCareerData = c?.system
          const levelStr = data?.level?.value
          const selectLevelStr = careerData?.level?.value
          const level = levelStr != null ? Number(levelStr) : 0
          const selectLevel =
            selectLevelStr != null ? Number(selectLevelStr) : 0
          return (
            data?.careergroup?.value === careerData?.careergroup?.value &&
            level <= selectLevel
          )
        })
        .sort((a, b) => {
          const aData: FoundryCareerData = a?.system
          const bData: FoundryCareerData = b?.system
          const aLevelStr = aData?.level?.value
          const bLevelStr = bData?.level?.value
          const aLevel = aLevelStr != null ? Number(aLevelStr) : 0
          const bLevel = bLevelStr != null ? Number(bLevelStr) : 0
          return aLevel - bLevel
        })
    } else {
      model.careers = [career]
    }
    model.careers.forEach((c, i) => {
      const data: FoundryCareerData = c?.system
      if (data?.current != null) {
        data.current.value = i === model.careers.length - 1
      }
      if (data?.complete != null) {
        data.complete.value = true
      }
    })

    const careerSkillsMap: { [group: string]: FoundryCareer } = {}
    for (let career of model.careers) {
      const data: FoundryCareerData = career.system
      const level = data?.level?.value ?? 0
      const group = data?.careergroup?.value ?? ''
      const careerSkill = careerSkillsMap[group]
      if (careerSkill == null) {
        careerSkillsMap[group] = career
      } else {
        const csData: FoundryCareerData = careerSkill?.system
        const csLevel = csData?.level?.value ?? 0
        if (level > csLevel) {
          careerSkillsMap[group] = career
        }
      }
    }
    model.careerForSkills.push(...Object.values(careerSkillsMap))
  }

  protected static async addStatus<T extends ActorModel>(
    model: ActorModelBuilder<T>,
    fromStanding: number | undefined = undefined,
    fromTier: string | undefined = undefined,
  ) {
    const careerData: FoundryCareerData = model.career?.system
    const statusTiers = ReferentialUtil.getStatusTiers()
    if (
      fromStanding != null &&
      fromTier != null &&
      statusTiers[fromTier] != null &&
      fromStanding >= 0
    ) {
      const tier = statusTiers[fromTier]
      model.status = `${tier} ${fromStanding}`
    } else if (careerData?.status != null) {
      const standing = careerData.status.standing
      const tier = statusTiers[careerData.status.tier]
      model.status = `${tier} ${standing}`
    }
  }

  protected static async addCityBornSkill<T extends ActorModel>(
    model: ActorModelBuilder<T>,
  ) {
    const loreSkill = await ReferentialUtil.findSkill('Lore')
    if (
      model.model.cityBorn != null &&
      model.model.cityBorn.length > 0 &&
      loreSkill != null
    ) {
      await this.addSkill(
        model,
        `${StringUtil.getSimpleName(loreSkill.name)} (${model.model.cityBorn})`,
      )
    }
  }

  protected static async addCareerSkill<T extends ActorModel>(
    model: ActorModelBuilder<T>,
  ) {
    for (let career of model.careerForSkills) {
      const data = career?.system
      await this.addSkills(model, data?.skills)
    }
  }

  protected static async addCareerTalents<T extends ActorModel>(
    model: ActorModelBuilder<T>,
  ) {
    for (let career of model.careers) {
      const data = career?.system ?? {}
      for (let talent of data?.talents ?? []) {
        // FIXME wait FR translate before make international function
        if (talent?.includes(' OR ')) {
          await this.addTalent(
            model,
            RandomUtil.getRandomValue(talent.split(' OR ')),
          )
        } else {
          await this.addTalent(model, talent)
        }
      }
    }
  }

  protected static async addAdvanceSkills<T extends ActorModel>(
    model: ActorModelBuilder<T>,
  ) {
    for (let career of model.careers) {
      const data = career?.system
      const level: number = Number(data.level?.value)
      const maxAdvance = level * 5
      for (let skill of data?.skills) {
        const sk = model.skills.find(
          (s) => s.name === skill && s.system.advances.value < maxAdvance,
        )
        if (sk != null) {
          sk.system.advances.value = maxAdvance
        }
      }
    }
  }

  protected static async addAdvanceChars<T extends ActorModel>(
    model: ActorModelBuilder<T>,
  ) {
    for (let career of model.careers) {
      const data = career?.system
      const level: number = Number(data.level?.value)
      const maxAdvance = level * 5
      for (let char of Object.keys(data?.characteristics)) {
        const ch = model.chars[char]
        if (ch != null && ch.advances < maxAdvance) {
          ch.advances = maxAdvance
        }
      }
    }
  }

  protected static async prepareClassTrappings<T extends ActorModel>(
    model: ActorModelBuilder<T>,
    trappings: string[],
  ) {
    const careerClasses: string[] = []
    model.careers.forEach((cp) => {
      const careerClass = ReferentialUtil.getClassKeyFromCareer(cp)
      if (careerClass != null && !careerClasses.includes(careerClass)) {
        careerClasses.push(careerClass)
      }
    })
    const classTrappings = ReferentialUtil.getClassTrappings()
    careerClasses.forEach((cc) => {
      const tps = classTrappings[cc]
      if (tps != null) {
        tps
          .split(',')
          .map((t) => t.toLowerCase().trim())
          .forEach((t) => {
            if (!trappings.includes(t)) {
              trappings.push(t)
            }
          })
      }
    })
  }

  protected static async prepareCareerTrappings<T extends ActorModel>(
    model: ActorModelBuilder<T>,
    trappings: string[],
  ) {
    for (let cp of model.careers) {
      for (let tr of cp.system.trappings.map((t: string) =>
        t.toLowerCase().trim(),
      )) {
        if (!trappings.includes(tr)) {
          trappings.push(tr)
        }
      }
    }
  }

  protected static async addTrappings<T extends ActorModel>(
    model: ActorModelBuilder<T>,
    trappings: string[],
  ) {
    const trappingCompendiums = await ReferentialUtil.getTrappingEntities(true)
    const trappingIds: string[] = []
    const hasMoney = model.trappings.some((t) => t.type === 'money')
    if (!hasMoney) {
      const moneyItems = await ReferentialUtil.getAllMoneyItems()
      model.trappings.push(...moneyItems)
    }
    for (let tr of trappings) {
      const trappings = await ReferentialUtil.findTrappings(
        tr,
        trappingCompendiums,
      )
      for (let trapping of trappings) {
        if (
          trapping != null &&
          !trappingIds.includes(trapping._id ?? '') &&
          trapping.type !== 'money'
        ) {
          trappingIds.push(trapping._id ?? '')
          model.trappings.push(trapping)
        }
      }
    }
  }

  protected static async addSkills<T extends ActorModel>(
    model: ActorModelBuilder<T>,
    names: string[],
  ) {
    if (names == null || names.length === 0) {
      return
    }
    for (let name of names) {
      await this.addSkill(model, name)
    }
  }

  protected static async addSkill<T extends ActorModel>(
    model: ActorModelBuilder<T>,
    name: string,
  ) {
    if (name == null || name.length === 0) {
      return
    }
    try {
      const skillToAdd = await ReferentialUtil.findSkill(name)
      if (
        !StringUtil.arrayIncludesDeburrIgnoreCase(
          model.skills.map((ms) => ms.name),
          skillToAdd.name,
        ) ||
        (skillToAdd.name.includes('(') &&
          StringUtil.includesDeburrIgnoreCase(
            skillToAdd.name,
            i18n().localize('WFRP4NPCGEN.item.any'),
          ))
      ) {
        model.skills.push(skillToAdd)
      }
    } catch (e) {
      console.warn('Cant find Skill : ' + name)
    }
  }

  protected static async addTalents<T extends ActorModel>(
    model: ActorModelBuilder<T>,
    names: string[],
  ) {
    if (names == null || names.length === 0) {
      return
    }
    for (let name of names) {
      await this.addTalent(model, name)
    }
  }

  protected static async addTalent<T extends ActorModel>(
    model: ActorModelBuilder<T>,
    name: string,
  ) {
    if (name == null || name.length === 0) {
      return
    }

    try {
      const talentToAdd = await ReferentialUtil.findTalent(name)
      if (
        !StringUtil.arrayIncludesDeburrIgnoreCase(
          model.talents.map((ms) => ms.name),
          talentToAdd.name,
        )
      ) {
        model.talents.push(talentToAdd)
      }
    } catch (e) {
      console.warn('Cant find Talent : ' + name)
    }
  }
}
