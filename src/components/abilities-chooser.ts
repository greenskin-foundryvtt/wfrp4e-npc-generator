import { AbstractChooser } from './abstract-chooser'
import { AbilityModel } from '../models/common/ability-model'
import { SourceSelectModel } from '../models/common/source-select-model'
import ChooserUtil from '../util/chooser-util'
import { i18n } from '../constant'
import RegisterSettings from '../util/register-settings'
import EntityUtil from '../util/entity-util'
import RandomUtil from '../util/random-util'
import { FoundrySkill } from '../types/foundry/skill'
import { FoundryTalent } from '../types/foundry/talent'
import { FoundryTrait } from '../types/foundry/trait'
import { FoundryItem } from '../types/foundry/item'

class Model {
  public skills: FoundrySkill[] = []
  public talents: FoundryTalent[] = []
  public traits: FoundryTrait[] = []

  constructor(
    skills: FoundrySkill[],
    talents: FoundryTalent[],
    traits: FoundryTrait[],
  ) {
    this.skills = skills
    this.talents = talents
    this.traits = traits
  }
}

export class AbilitiesChooser extends AbstractChooser<
  Model,
  {
    abilitiesTemplate: () => string
    abilityTemplate: () => string
    skills: SourceSelectModel[]
    talents: SourceSelectModel[]
    traits: SourceSelectModel[]
    selectedSkills: AbilityModel[]
    selectedTalents: AbilityModel[]
    selectedTraits: AbilityModel[]
  }
> {
  private selectedSkill: SourceSelectModel | null = null
  private selectedTalent: SourceSelectModel | null = null
  private selectedTrait: SourceSelectModel | null = null

  private callback: (
    skills: FoundrySkill[],
    talents: FoundryTalent[],
    traits: FoundryTrait[],
  ) => void

  constructor(
    object: Model,
    skills: SourceSelectModel[],
    talents: SourceSelectModel[],
    traits: SourceSelectModel[],
    callback: (
      skills: FoundrySkill[],
      talents: FoundryTalent[],
      traits: FoundryTrait[],
    ) => void,
    previousCallback?: (() => void) | null,
    options?: Partial<FormApplicationOptions>,
  ) {
    super(object, previousCallback, options)
    this.model.abilitiesTemplate = () =>
      `modules/${RegisterSettings.moduleName}/templates/ability-rows.html`
    this.model.abilityTemplate = () =>
      `modules/${RegisterSettings.moduleName}/templates/ability-row.html`

    this.callback = callback
    this.model.skills = skills
    this.model.talents = talents
    this.model.traits = traits

    if (skills?.length > 0) {
      this.selectedSkill = skills[0]
    }
    if (talents?.length > 0) {
      this.selectedTalent = talents[0]
    }
    if (traits?.length > 0) {
      this.selectedTrait = traits[0]
    }

    this.initSelectedFromModel()
  }

  public static get defaultOptions(): FormApplicationOptions {
    return mergeObject(super.defaultOptions, {
      id: 'abilities-chooser',
      title: i18n().localize('WFRP4NPCGEN.creatures.abilities.select.title'),
      template: `modules/${RegisterSettings.moduleName}/templates/abilities-chooser.html`,
      width: 550,
    })
  }

  public activateListeners(html: JQuery) {
    super.activateListeners(html)

    this.handleChange(html, '#abilities-select-skills', (event) => {
      const key = event.currentTarget.value
      this.selectedSkill = this.model.skills.find((s) => s.key === key) ?? null
      this.selectAbility(this.selectedSkill, this.model.skills)
    })
    this.handleChange(html, '#abilities-select-talents', (event) => {
      const key = event.currentTarget.value
      this.selectedTalent =
        this.model.talents.find((t) => t.key === key) ?? null
      this.selectAbility(this.selectedTalent, this.model.talents)
    })
    this.handleChange(html, '#abilities-select-traits', (event) => {
      const key = event.currentTarget.value
      this.selectedTrait = this.model.traits.find((t) => t.key === key) ?? null
      this.selectAbility(this.selectedTrait, this.model.traits)
    })

    this.handleClick(html, '#abilities-add-skills', () => {
      this.addAbility(
        this.selectedSkill,
        this.model.selectedSkills,
        this.model.data.skills,
      )
      this.render()
    })
    this.handleClick(html, '#abilities-add-talents', () => {
      this.addAbility(
        this.selectedTalent,
        this.model.selectedTalents,
        this.model.data.talents,
      )
      this.render()
    })
    this.handleClick(html, '#abilities-add-traits', () => {
      this.addAbility(
        this.selectedTrait,
        this.model.selectedTraits,
        this.model.data.traits,
      )
      this.render()
    })
    this.handleClick(html, '.ability-remove-skills', (event) => {
      const key = event.currentTarget.value
      this.removeAbility(key, this.model.selectedSkills, this.model.data.skills)
      this.render()
    })
    this.handleClick(html, '.ability-remove-talents', (event) => {
      const key = event.currentTarget.value
      this.removeAbility(
        key,
        this.model.selectedTalents,
        this.model.data.talents,
      )
      this.render()
    })
    this.handleClick(html, '.ability-remove-traits', (event) => {
      const key = event.currentTarget.value
      this.removeAbility(key, this.model.selectedTraits, this.model.data.traits)
      this.render()
    })

    this.handleInput(html, '.ability-advances-input-skills', (event) => {
      this.updateAdvance(
        event,
        this.model.selectedSkills,
        this.model.data.skills,
      )
    })
    this.handleInput(html, '.ability-advances-input-talents', (event) => {
      this.updateAdvance(
        event,
        this.model.selectedTalents,
        this.model.data.talents,
      )
    })

    this.handleInput(html, '.ability-group-input-skills', (event) => {
      this.updateGroup(event, this.model.selectedSkills, this.model.data.skills)
    })
    this.handleInput(html, '.ability-group-input-talents', (event) => {
      this.updateGroup(
        event,
        this.model.selectedTalents,
        this.model.data.talents,
      )
    })
    this.handleInput(html, '.ability-group-input-traits', (event) => {
      this.updateGroup(event, this.model.selectedTraits, this.model.data.traits)
    })

    this.handleInput(html, '.ability-spec-input-traits', (event) => {
      this.updateSpec(event, this.model.selectedTraits, this.model.data.traits)
    })
  }

  protected isValid(_data: Model): boolean {
    return true
  }

  protected yes(data: Model) {
    this.callback(data?.skills, data?.talents, data?.traits)
  }

  public static async editAbilities(
    initSkills: FoundrySkill[],
    initTalents: FoundryTalent[],
    initTraits: FoundryTrait[],
    callback: (
      skills: FoundrySkill[],
      talents: FoundryTalent[],
      traits: FoundryTrait[],
    ) => void,
    undo?: () => void,
  ) {
    const model = new Model(
      initSkills ?? [],
      initTalents ?? [],
      initTraits ?? [],
    )

    const skills: SourceSelectModel[] =
      await ChooserUtil.getEditSkills(initSkills)
    const talents: SourceSelectModel[] =
      await ChooserUtil.getEditTalents(initTalents)
    const traits: SourceSelectModel[] =
      await ChooserUtil.getEditTraits(initTraits)

    new AbilitiesChooser(model, skills, talents, traits, callback, undo).render(
      true,
    )
  }

  public initSelectedFromModel() {
    this.model.selectedSkills =
      this.model.data.skills?.map(
        (s: FoundrySkill) => new AbilityModel(s, s.displayName ?? s.name),
      ) ?? []
    this.model.selectedTalents =
      this.model.data.talents?.map(
        (t: FoundryTalent) => new AbilityModel(t, t.displayName ?? t.name),
      ) ?? []
    this.model.selectedTraits =
      this.model.data.traits?.map(
        (t: FoundryTrait) => new AbilityModel(t, t.DisplayName ?? t.name),
      ) ?? []
  }

  private selectAbility(
    ability: SourceSelectModel | null,
    abilities: SourceSelectModel[],
  ) {
    abilities.forEach((a) => {
      a.selected = ability?.key === a.key
    })
  }

  private addAbility(
    ability: SourceSelectModel | null,
    selectedAbilities: AbilityModel[],
    abilities: FoundryItem[],
  ) {
    if (ability != null) {
      const hasGroupName = EntityUtil.hasGroup(ability.source)
      const abilityData = hasGroupName
        ? duplicate(ability.source as FoundryItem)
        : (ability.source as FoundryItem)
      if (hasGroupName) {
        abilityData._id = RandomUtil.getRandomId()
        abilityData.pack = ability.source.pack as string
      }

      if (
        selectedAbilities.find(
          (a) =>
            (!hasGroupName && a.id === ability.key) ||
            (hasGroupName && a.name === ability.value),
        ) == null
      ) {
        selectedAbilities.push(new AbilityModel(abilityData, ability.value))
        abilities.push(abilityData)
      }
    }
  }

  private removeAbility(
    id: string,
    selectedAbilities: AbilityModel[],
    abilities: FoundryItem[],
  ) {
    if (id?.length > 0) {
      let indexOfSelected = selectedAbilities.findIndex((a) => a.id === id)
      if (indexOfSelected >= 0) {
        selectedAbilities.splice(indexOfSelected, 1)
      }
      indexOfSelected = abilities.findIndex((a) => a._id === id)
      if (indexOfSelected >= 0) {
        abilities.splice(indexOfSelected, 1)
      }
    }
  }

  private updateAdvance(
    event: JQuery.EventBase,
    selectedAbilities: AbilityModel[],
    abilities: FoundryItem[],
  ) {
    const key = event.currentTarget.getAttribute('data-id')
    const value = Number(event.currentTarget.value)
    const selectedAbility = selectedAbilities.find((a) => a.id === key)
    if (selectedAbility != null) {
      selectedAbility.advance = value
      const ability = abilities.find((a) => a._id === key)
      if (ability != null) {
        ability.system.advances.value = value
      }
    }
  }

  private updateGroup(
    event: JQuery.EventBase,
    selectedAbilities: AbilityModel[],
    abilities: FoundryItem[],
  ) {
    const key = event.currentTarget.getAttribute('data-id')
    const value = (event.currentTarget.value ?? '')?.trim() ?? ''
    const selectedAbility = selectedAbilities.find((a) => a.id === key)
    if (selectedAbility != null) {
      const name = `${selectedAbility.simpleName} (${value})`
      selectedAbility.groupName = value
      selectedAbility.name = name
      const ability = abilities.find((a) => a._id === key)
      if (ability != null) {
        ability.name = name
      }
    }
  }

  private updateSpec(
    event: JQuery.EventBase,
    selectedAbilities: AbilityModel[],
    abilities: FoundryItem[],
  ) {
    const key = event.currentTarget.getAttribute('data-id')
    const value = (event.currentTarget.value ?? '')?.trim() ?? ''
    const selectedAbility = selectedAbilities.find((a) => a.id === key)
    if (selectedAbility != null) {
      selectedAbility.specification = value
      const ability = abilities.find((a) => a._id === key)
      if (ability != null) {
        ability.system.specification.value = value
      }
    }
  }
}
