import { AbstractChooser } from './abstract-chooser'
import { GroupedSourceSelectModel } from '../models/common/grouped-source-select-model'
import { SourcedModel } from '../models/common/sourced-model'
import { SourceSelectModel } from '../models/common/source-select-model'
import { i18n } from '../constant'
import RegisterSettings from '../util/register-settings'
import ChooserUtil from '../util/chooser-util'
import { FoundrySpell } from '../types/foundry/spell'
import { FoundryPrayer } from '../types/foundry/prayer'
import { FoundryItem } from '../types/foundry/item'

class Model {
  public spells: FoundrySpell[] = []
  public prayers: FoundryPrayer[] = []

  constructor(spells: FoundrySpell[], prayers: FoundryPrayer[]) {
    this.spells = spells
    this.prayers = prayers
  }
}

export class MagicChooser extends AbstractChooser<
  Model,
  {
    magicsTemplate: () => string
    magicTemplate: () => string
    spells: GroupedSourceSelectModel[]
    prayers: GroupedSourceSelectModel[]
    selectedSpells: SourcedModel[]
    selectedPrayers: SourcedModel[]
  }
> {
  private selectedSpell: SourceSelectModel | null = null
  private selectedPrayer: SourceSelectModel | null = null

  private callback: (spells: FoundrySpell[], prayers: FoundryPrayer[]) => void

  constructor(
    object: Model,
    spells: GroupedSourceSelectModel[],
    prayers: GroupedSourceSelectModel[],
    callback: (spells: FoundrySpell[], prayers: FoundryPrayer[]) => void,
    previousCallback?: (() => void) | null,
    options?: Partial<FormApplicationOptions>,
  ) {
    super(object, previousCallback, options)
    this.model.magicsTemplate = () =>
      `modules/${RegisterSettings.moduleName}/templates/magic-rows.html`
    this.model.magicTemplate = () =>
      `modules/${RegisterSettings.moduleName}/templates/magic-row.html`

    this.callback = callback
    this.model.spells = spells
    this.model.prayers = prayers

    if (spells?.length > 0 && spells[0].items?.length > 0) {
      this.selectedSpell = spells[0].items[0]
    }
    if (prayers?.length > 0 && prayers[0].items?.length > 0) {
      this.selectedPrayer = prayers[0].items[0]
    }

    this.initSelectedFromModel()
  }

  public static get defaultOptions(): FormApplicationOptions {
    return mergeObject(super.defaultOptions, {
      id: 'magic-chooser',
      title: i18n().localize('WFRP4NPCGEN.select.magics.title'),
      template: `modules/${RegisterSettings.moduleName}/templates/magic-chooser.html`,
      width: 550,
    })
  }

  public activateListeners(html: JQuery) {
    super.activateListeners(html)

    this.handleChange(html, '#magics-select-spells', (event) => {
      const group =
        event.currentTarget.selectedOptions?.length > 0
          ? event.currentTarget.selectedOptions[0].getAttribute('data-group')
          : ''
      const key = event.currentTarget.value
      const groupedSpells = this.model.spells.find((gs) => gs.group === group)
      this.selectedSpell =
        groupedSpells?.items?.find((s) => s.key === key) ?? null
      this.selectMagic(this.selectedSpell, this.model.spells)
    })
    this.handleChange(html, '#magics-select-prayers', (event) => {
      const group =
        event.currentTarget.selectedOptions?.length > 0
          ? event.currentTarget.selectedOptions[0].getAttribute('data-group')
          : ''
      const key = event.currentTarget.value
      const groupedPrayers = this.model.prayers.find((gp) => gp.group === group)
      this.selectedPrayer =
        groupedPrayers?.items?.find((p) => p.key === key) ?? null
      this.selectMagic(this.selectedPrayer, this.model.prayers)
    })

    this.handleClick(html, '#magics-add-spells', () => {
      this.addMagic(
        this.selectedSpell,
        this.model.selectedSpells,
        this.model.data.spells,
      )
      this.render()
    })
    this.handleClick(html, '#magics-add-prayers', () => {
      this.addMagic(
        this.selectedPrayer,
        this.model.selectedPrayers,
        this.model.data.prayers,
      )
      this.render()
    })

    this.handleClick(html, '.magic-remove-spells', (event) => {
      const key = event.currentTarget.value
      this.removeMagic(key, this.model.selectedSpells, this.model.data.spells)
      this.render()
    })
    this.handleClick(html, '.magic-remove-prayers', (event) => {
      const key = event.currentTarget.value
      this.removeMagic(key, this.model.selectedPrayers, this.model.data.prayers)
      this.render()
    })
  }

  protected isValid(_data: Model): boolean {
    return true
  }

  protected yes(data: Model) {
    this.callback(data?.spells, data?.prayers)
  }

  public static async editMagics(
    initSpells: FoundrySpell[],
    initPrayers: FoundryPrayer[],
    callback: (spells: FoundrySpell[], prayers: FoundryPrayer[]) => void,
    undo?: () => void,
  ) {
    const model = new Model(initSpells ?? [], initPrayers ?? [])

    const spells: GroupedSourceSelectModel[] = await ChooserUtil.getEditSpells()
    const prayers: GroupedSourceSelectModel[] =
      await ChooserUtil.getEditPrayers()

    new MagicChooser(model, spells, prayers, callback, undo).render(true)
  }

  public initSelectedFromModel() {
    this.model.selectedSpells =
      this.model.data.spells?.map(
        (s) => new SourcedModel(s, s.displayName ?? s.name),
      ) ?? []
    this.model.selectedPrayers =
      this.model.data.prayers?.map(
        (t) => new SourcedModel(t, t.displayName ?? t.name),
      ) ?? []
  }

  private selectMagic(
    magic: SourceSelectModel | null,
    magics: GroupedSourceSelectModel[],
  ) {
    magics.forEach((gm) => {
      gm.items.forEach((m) => {
        m.selected = magic?.key === m.key
      })
    })
  }

  private addMagic(
    magic: SourceSelectModel | null,
    selectedMagics: SourcedModel[],
    magics: FoundryItem[],
  ) {
    if (magic != null) {
      const magicData = magic.source as FoundryItem

      if (selectedMagics.find((a) => a.id === magic.key) == null) {
        selectedMagics.push(new SourcedModel(magicData, magic.value))
        magics.push(magicData)
      }
    }
  }

  private removeMagic(
    id: string,
    selectedMagics: SourcedModel[],
    magics: FoundryItem[],
  ) {
    if (id?.length > 0) {
      let indexOfSelected = selectedMagics.findIndex((a) => a.id === id)
      if (indexOfSelected >= 0) {
        selectedMagics.splice(indexOfSelected, 1)
      }
      indexOfSelected = magics.findIndex((a) => a._id === id)
      if (indexOfSelected >= 0) {
        magics.splice(indexOfSelected, 1)
      }
    }
  }
}
