import { AbstractChooser } from './abstract-chooser'
import { SourceSelectModel } from '../models/common/source-select-model'
import RegisterSettings from '../util/register-settings'
import { i18n } from '../constant'
import ChooserUtil from '../util/chooser-util'
import { GroupedSourceSelectModel } from '../models/common/grouped-source-select-model'
import RandomUtil from '../util/random-util'
import { TrappingModel } from '../models/common/trapping-model'
import { FoundryTrapping } from '../types/foundry/trapping'

class Model {
  public trappings: FoundryTrapping[] = []

  constructor(trappings: FoundryTrapping[]) {
    this.trappings = trappings
  }
}

export class TrappingChooser extends AbstractChooser<
  Model,
  {
    trappings: GroupedSourceSelectModel[]
    selectedTrappings: TrappingModel[]
  }
> {
  private selectedTrapping: SourceSelectModel | null = null

  private callback: (trappings: FoundryTrapping[]) => void

  constructor(
    object: Model,
    trappings: GroupedSourceSelectModel[],
    callback: (trappings: FoundryTrapping[]) => void,
    previousCallback?: (() => void) | null,
    options?: Partial<FormApplicationOptions>,
  ) {
    super(object, previousCallback, options)

    this.callback = callback
    this.model.trappings = trappings

    if (trappings?.length > 0 && trappings[0].items?.length > 0) {
      this.selectedTrapping = trappings[0].items[0]
    }

    this.initSelectedFromModel()
  }

  public static get defaultOptions(): FormApplicationOptions {
    return mergeObject(super.defaultOptions, {
      id: 'trapping-chooser',
      title: i18n().localize('WFRP4NPCGEN.select.trappings.title'),
      template: `modules/${RegisterSettings.moduleName}/templates/trapping-chooser.html`,
      width: 550,
    })
  }

  public activateListeners(html: JQuery) {
    super.activateListeners(html)

    this.handleChange(html, '#trappings-select', (event) => {
      const key = event.currentTarget.value
      this.selectTrapping(key)
    })

    this.handleClick(html, '#trappings-add', () => {
      this.addTrapping()
      this.render()
    })

    this.handleClick(html, '.trapping-remove', (event) => {
      const key = event.currentTarget.value
      this.removeTrapping(key)
      this.render()
    })

    this.handleInput(html, '.trapping-quantity-input', (event) => {
      this.updateQuantity(event)
    })
  }

  protected isValid(_data: Model): boolean {
    return true
  }

  protected yes(data: Model) {
    this.callback(data?.trappings)
  }

  public static async editTrappings(
    initTrappings: FoundryTrapping[],
    callback: (trappings: FoundryTrapping[]) => void,
    undo?: () => void,
  ) {
    const model = new Model(initTrappings ?? [])

    const trappings: GroupedSourceSelectModel[] =
      await ChooserUtil.getEditTrappings()

    new TrappingChooser(model, trappings, callback, undo).render(true)
  }

  public initSelectedFromModel() {
    this.model.selectedTrappings =
      this.model.data.trappings?.map(
        (t) => new TrappingModel(t, t.displayName ?? t.name),
      ) ?? []
  }

  private selectTrapping(key: string) {
    this.selectedTrapping = null
    this.model.trappings.forEach((gt) => {
      gt.items.forEach((t) => {
        const selected = t.key === key
        if (selected) {
          this.selectedTrapping = t
        }
        return selected
      })
    })
  }

  private addTrapping() {
    if (this.selectedTrapping != null) {
      const dSource = duplicate(
        this.selectedTrapping?.source as FoundryTrapping,
      )
      dSource._id = RandomUtil.getRandomId()
      dSource.pack = this.selectedTrapping?.source?.pack as string

      this.model.selectedTrappings.push(
        new TrappingModel(dSource, this.selectedTrapping.value),
      )
      this.model.data.trappings.push(dSource)
    }
  }

  private removeTrapping(id: string) {
    if (id?.length > 0) {
      let indexOfSelected = this.model.selectedTrappings.findIndex(
        (a) => a.id === id,
      )
      if (indexOfSelected >= 0) {
        this.model.selectedTrappings.splice(indexOfSelected, 1)
      }
      indexOfSelected = this.model.data.trappings.findIndex((a) => a._id === id)
      if (indexOfSelected >= 0) {
        this.model.data.trappings.splice(indexOfSelected, 1)
      }
    }
  }

  private updateQuantity(event: JQuery.EventBase) {
    const key = event.currentTarget.getAttribute('data-id')
    const value = Number(event.currentTarget.value)
    const selectedTrapping = this.model.selectedTrappings.find(
      (a) => a.id === key,
    )
    if (selectedTrapping != null) {
      selectedTrapping.quantity = value
      const trapping = this.model.data.trappings.find((a) => a._id === key)
      if (trapping != null) {
        trapping.system.quantity.value = value
      }
    }
  }
}
