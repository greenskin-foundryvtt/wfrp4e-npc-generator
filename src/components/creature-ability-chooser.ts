import { AbstractChooser } from './abstract-chooser'
import CreatureAbilities from '../models/creature/creature-abilities'
import { SourceSelectModel } from '../models/common/source-select-model'
import { CreatureAbilityModel } from '../models/creature/creature-ability-model'
import RegisterSettings from '../util/register-settings'
import { i18n } from '../constant'
import ChooserUtil from '../util/chooser-util'
import EntityUtil from '../util/entity-util'
import RandomUtil from '../util/random-util'
import { SelectModel } from '../models/common/select-model'
import ReferentialUtil from '../util/referential-util'
import CompendiumUtil from '../util/compendium-util'
import { FoundrySkill } from '../types/foundry/skill'
import { FoundryTalent } from '../types/foundry/talent'
import { FoundryTrait } from '../types/foundry/trait'
import { FoundryItem } from '../types/foundry/item'

class Model {
  public abilities: CreatureAbilities

  constructor(abilities: CreatureAbilities) {
    this.abilities = abilities
  }
}

export class CreatureAbilityChooser extends AbstractChooser<
  Model,
  {
    abilitiesTemplate: () => string
    abilityTemplate: () => string
    abilityAttrTemplate: () => string
    abilityToggleAttrTemplate: () => string
    abilitySelectAttrTemplate: () => string
    skills: SourceSelectModel[]
    talents: SourceSelectModel[]
    traits: SourceSelectModel[]
    selectedSkills: CreatureAbilityModel[]
    selectedTalents: CreatureAbilityModel[]
    selectedTraits: CreatureAbilityModel[]
    species: SelectModel[]
    sizes: SelectModel[]
  }
> {
  private selectedSkill: SourceSelectModel | null = null
  private selectedTalent: SourceSelectModel | null = null
  private selectedTrait: SourceSelectModel | null = null

  private callback: (abilities: CreatureAbilities) => void

  constructor(
    object: Model,
    skills: SourceSelectModel[],
    talents: SourceSelectModel[],
    traits: SourceSelectModel[],
    callback: (abilities: CreatureAbilities) => void,
    previousCallback?: (() => void) | null,
    options?: Partial<FormApplicationOptions>,
  ) {
    super(object, previousCallback, options)
    this.model.abilitiesTemplate = () =>
      `modules/${RegisterSettings.moduleName}/templates/creature-ability-rows.html`
    this.model.abilityTemplate = () =>
      `modules/${RegisterSettings.moduleName}/templates/creature-ability-row.html`
    this.model.abilityAttrTemplate = () =>
      `modules/${RegisterSettings.moduleName}/templates/creature-ability-attr.html`
    this.model.abilityToggleAttrTemplate = () =>
      `modules/${RegisterSettings.moduleName}/templates/creature-ability-toggle-attr.html`
    this.model.abilitySelectAttrTemplate = () =>
      `modules/${RegisterSettings.moduleName}/templates/creature-ability-select-attr.html`

    this.callback = callback
    this.model.skills = skills
    this.model.talents = talents
    this.model.traits = traits

    if (skills?.length > 0) {
      this.selectedSkill = skills[0]
    }
    if (talents?.length > 0) {
      this.selectedTalent = talents[0]
    }
    if (traits?.length > 0) {
      this.selectedTrait = traits[0]
    }

    this.initSpecies()
    this.initSizes()
    this.initSelectedFromModel()
  }

  public static get defaultOptions(): FormApplicationOptions {
    return mergeObject(super.defaultOptions, {
      id: 'creature-abilities-chooser',
      title: i18n().localize('WFRP4NPCGEN.creatures.abilities.select.title'),
      template: `modules/${RegisterSettings.moduleName}/templates/creature-abilities-chooser.html`,
      width: 550,
    })
  }

  public activateListeners(html: JQuery) {
    super.activateListeners(html)

    this.handleChange(html, '#creature-abilities-select-skills', (event) => {
      const key = event.currentTarget.value
      this.selectedSkill = this.model.skills.find((s) => s.key === key) ?? null
      this.selectAbility(this.selectedSkill, this.model.skills)
    })
    this.handleChange(html, '#creature-abilities-select-talents', (event) => {
      const key = event.currentTarget.value
      this.selectedTalent =
        this.model.talents.find((t) => t.key === key) ?? null
      this.selectAbility(this.selectedTalent, this.model.talents)
    })
    this.handleChange(html, '#creature-abilities-select-traits', (event) => {
      const key = event.currentTarget.value
      this.selectedTrait = this.model.traits.find((t) => t.key === key) ?? null
      this.selectAbility(this.selectedTrait, this.model.traits)
    })

    this.handleClick(html, '#creature-abilities-add-skills', () => {
      this.addAbility(
        this.selectedSkill,
        this.model.selectedSkills,
        this.model.data.abilities.skills,
      )
      this.render()
    })
    this.handleClick(html, '#creature-abilities-add-talents', () => {
      this.addAbility(
        this.selectedTalent,
        this.model.selectedTalents,
        this.model.data.abilities.talents,
      )
      this.render()
    })
    this.handleClick(html, '#creature-abilities-add-traits', () => {
      this.addAbility(
        this.selectedTrait,
        this.model.selectedTraits,
        this.model.data.abilities.traits,
        true,
      )
      this.render()
    })
    this.handleClick(html, '.creature-ability-remove-skills', (event) => {
      const key = event.currentTarget.value
      this.removeAbility(
        key,
        this.model.selectedSkills,
        this.model.data.abilities.skills,
      )
      this.render()
    })
    this.handleClick(html, '.creature-ability-remove-talents', (event) => {
      const key = event.currentTarget.value
      this.removeAbility(
        key,
        this.model.selectedTalents,
        this.model.data.abilities.talents,
      )
      this.render()
    })
    this.handleClick(html, '.creature-ability-remove-traits', (event) => {
      const key = event.currentTarget.value
      this.removeAbility(
        key,
        this.model.selectedTraits,
        this.model.data.abilities.traits,
      )
      this.render()
    })

    this.handleInput(
      html,
      '.creature-ability-advances-input-skills',
      (event) => {
        this.updateAdvance(
          event,
          this.model.selectedSkills,
          this.model.data.abilities.skills,
        )
      },
    )
    this.handleInput(
      html,
      '.creature-ability-advances-input-talents',
      (event) => {
        this.updateAdvance(
          event,
          this.model.selectedTalents,
          this.model.data.abilities.talents,
        )
      },
    )

    this.handleInput(html, '.creature-ability-group-input-skills', (event) => {
      this.updateGroup(
        event,
        this.model.selectedSkills,
        this.model.data.abilities.skills,
      )
    })
    this.handleInput(html, '.creature-ability-group-input-talents', (event) => {
      this.updateGroup(
        event,
        this.model.selectedTalents,
        this.model.data.abilities.talents,
      )
    })
    this.handleInput(html, '.creature-ability-group-input-traits', (event) => {
      this.updateGroup(
        event,
        this.model.selectedTraits,
        this.model.data.abilities.traits,
      )
    })

    this.handleInput(html, '.creature-ability-spec-input-traits', (event) => {
      this.updateSpec(
        event,
        this.model.selectedTraits,
        this.model.data.abilities.traits,
      )
    })

    this.handleClick(
      html,
      '.creature-ability-include-checkbox-traits',
      (event) => {
        this.toggleInclude(
          event,
          this.model.selectedTraits,
          this.model.data.abilities.traits,
        )
      },
    )

    this.handleInput(html, '.creature-ability-attr', (event) => {
      this.updateAbilitiesAttr(event)
    })

    this.handleClick(html, '.creature-ability-checkbox-attr', (event) => {
      this.toggleAbilitiesAttr(event)
    })

    this.handleChange(html, '.creature-ability-select-attr', (event) => {
      this.updateAbilitiesSelectAttr(event)
    })
  }

  protected isValid(data: Model): boolean {
    return data.abilities != null
  }

  protected yes(data: Model) {
    this.callback(data?.abilities)
  }

  public static async selectCreatureAbilities(
    initAbilities: CreatureAbilities,
    callback: (abilities: CreatureAbilities) => void,
    undo?: () => void,
  ) {
    const model = new Model(initAbilities)

    const skills: SourceSelectModel[] = await ChooserUtil.getEditSkills(
      initAbilities.skills,
    )
    const talents: SourceSelectModel[] = await ChooserUtil.getEditTalents(
      initAbilities.talents,
    )
    const traits: SourceSelectModel[] = await ChooserUtil.getEditTraits(
      initAbilities.traits,
      true,
    )

    new CreatureAbilityChooser(
      model,
      skills,
      talents,
      traits,
      callback,
      undo,
    ).render(true)
  }

  public initSpecies() {
    const species: SelectModel[] = []
    species.push(
      new SelectModel(
        '',
        '',
        this.model.data.abilities.speciesKey == null ||
          this.model.data.abilities.speciesKey.length === 0,
      ),
    )
    const speciesMap = ReferentialUtil.getSpeciesMap()
    Object.keys(ReferentialUtil.getSpeciesMap()).forEach((key) => {
      species.push(
        new SelectModel(
          key,
          speciesMap[key],
          key === this.model.data.abilities.speciesKey,
        ),
      )
    })
    this.model.species = species
  }

  public initSizes() {
    const sizes: SelectModel[] = []
    const sizeMap = CompendiumUtil.getSizes()
    Object.keys(CompendiumUtil.getSizes()).forEach((key) => {
      sizes.push(
        new SelectModel(
          key,
          sizeMap[key],
          key === this.model.data.abilities.sizeKey,
        ),
      )
    })
    this.model.sizes = sizes
  }

  public initSelectedFromModel() {
    this.model.selectedSkills =
      this.model.data.abilities.skills?.map(
        (s: FoundrySkill) =>
          new CreatureAbilityModel(s, s.displayName ?? s.name, true),
      ) ?? []
    this.model.selectedTalents =
      this.model.data.abilities.talents?.map(
        (t: FoundryTalent) =>
          new CreatureAbilityModel(t, t.displayName ?? t.name, true),
      ) ?? []
    this.model.selectedTraits =
      this.model.data.abilities.traits?.map(
        (t: FoundryTrait) =>
          new CreatureAbilityModel(
            t,
            t.DisplayName ?? t.name,
            !t.system?.disabled ?? true,
          ),
      ) ?? []
  }

  private selectAbility(
    ability: SourceSelectModel | null,
    abilities: SourceSelectModel[],
  ) {
    abilities.forEach((a) => {
      a.selected = ability?.key === a.key
    })
  }

  private addAbility(
    ability: SourceSelectModel | null,
    selectedAbilities: CreatureAbilityModel[],
    abilities: FoundryItem[],
    manageExclude = false,
  ) {
    if (ability != null) {
      const hasGroupName = EntityUtil.hasGroup(ability.source as FoundryItem)
      const hasSpec = EntityUtil.hasSpec(ability.source as FoundryItem)
      const abilityData =
        hasGroupName || hasSpec
          ? duplicate(ability.source as FoundryItem)
          : (ability.source as FoundryItem)
      if (hasGroupName || hasSpec) {
        abilityData._id = RandomUtil.getRandomId()
        abilityData.pack = ability.source.pack as string
      }
      const specification = (ability.source as FoundryItem).system
        ?.specification?.value

      if (
        selectedAbilities.find(
          (a) =>
            (!hasGroupName && !hasSpec && a.id === ability.key) ||
            (hasGroupName && !hasSpec && a.name === ability.value) ||
            (!hasGroupName && hasSpec && a.specification === specification) ||
            (hasGroupName &&
              hasSpec &&
              a.name === ability.value &&
              a.specification === specification),
        ) == null
      ) {
        if (manageExclude) {
          abilityData.system.disabled = false
        }
        selectedAbilities.push(
          new CreatureAbilityModel(abilityData, ability.value, true),
        )
        abilities.push(abilityData)
      }
    }
  }

  private removeAbility(
    id: string,
    selectedAbilities: CreatureAbilityModel[],
    abilities: FoundryItem[],
  ) {
    if (id?.length > 0) {
      let indexOfSelected = selectedAbilities.findIndex((a) => a.id === id)
      if (indexOfSelected >= 0) {
        selectedAbilities.splice(indexOfSelected, 1)
      }
      indexOfSelected = abilities.findIndex((a) => a._id === id)
      if (indexOfSelected >= 0) {
        abilities.splice(indexOfSelected, 1)
      }
    }
  }

  private updateAdvance(
    event: JQuery.EventBase,
    selectedAbilities: CreatureAbilityModel[],
    abilities: FoundryItem[],
  ) {
    const key = event.currentTarget.getAttribute('data-id')
    const value = Number(event.currentTarget.value)
    const selectedAbility = selectedAbilities.find((a) => a.id === key)
    if (selectedAbility != null) {
      selectedAbility.advance = value
      const ability = abilities.find((a) => a._id === key)
      if (ability != null) {
        ability.system.advances.value = value
      }
    }
  }

  private updateGroup(
    event: JQuery.EventBase,
    selectedAbilities: CreatureAbilityModel[],
    abilities: FoundryItem[],
  ) {
    const key = event.currentTarget.getAttribute('data-id')
    const value = (event.currentTarget.value ?? '')?.trim() ?? ''
    const selectedAbility = selectedAbilities.find((a) => a.id === key)
    if (selectedAbility != null) {
      const name = `${selectedAbility.simpleName} (${value})`
      selectedAbility.groupName = value
      selectedAbility.name = name
      const ability = abilities.find((a) => a._id === key)
      if (ability != null) {
        ability.name = name
      }
    }
  }

  private updateSpec(
    event: JQuery.EventBase,
    selectedAbilities: CreatureAbilityModel[],
    abilities: FoundryItem[],
  ) {
    const key = event.currentTarget.getAttribute('data-id')
    const value = (event.currentTarget.value ?? '')?.trim() ?? ''
    const selectedAbility = selectedAbilities.find((a) => a.id === key)
    if (selectedAbility != null) {
      selectedAbility.specification = value
      const ability = abilities.find((a) => a._id === key)
      if (ability != null) {
        ability.system.specification.value = value
      }
    }
  }

  private toggleInclude(
    event: JQuery.ClickEvent,
    selectedAbilities: CreatureAbilityModel[],
    abilities: FoundryItem[],
  ) {
    const key = event.currentTarget.value
    const selectedAbility = selectedAbilities.find((a) => a.id === key)
    if (selectedAbility != null) {
      selectedAbility.include = !selectedAbility.include
      const ability = abilities.find((a) => a._id === key)
      if (ability != null) {
        ability.system.disabled = !selectedAbility.include
      }
    }
  }

  private updateAbilitiesAttr(event: JQuery.EventBase) {
    const value = event.currentTarget.value
    const key = event.currentTarget.getAttribute('data-attr')
    this.model.data.abilities[key] = String(value ?? '')
  }

  private toggleAbilitiesAttr(event: JQuery.ClickEvent) {
    const key = event.currentTarget.value
    this.model.data.abilities[key] = !this.model.data.abilities[key]
  }

  private updateAbilitiesSelectAttr(event: JQuery.ChangeEvent) {
    const key = event.currentTarget.value
    const attrKey = event.currentTarget.getAttribute('data-attr')
    const models = event.currentTarget.getAttribute('data-models')
    const selectModels: SelectModel[] = this.model[models]
    selectModels.forEach((sm) => {
      sm.selected = key === sm.key
    })
    this.model.data.abilities[attrKey] = key
  }
}
