import { AbstractChooser } from './abstract-chooser'
import { SelectModel } from '../models/common/select-model'
import ReferentialUtil from '../util/referential-util'
import { i18n } from '../constant'
import RegisterSettings from '../util/register-settings'
import RandomUtil from '../util/random-util'
import ChooserUtil from '../util/chooser-util'

class Model {
  public speciesKey: string
  public cityBorn: string | null
  public nativeTongue: string | null

  constructor(
    speciesKey: string,
    cityBorn: string | null,
    nativeTongue: string | null,
  ) {
    this.speciesKey = speciesKey
    this.cityBorn = cityBorn
    this.nativeTongue = nativeTongue
  }
}

export class CreaturePnjChooser extends AbstractChooser<
  Model,
  {
    species: SelectModel[]
    nativeTongues: string[]
  }
> {
  private speciesKeys = Object.keys(ReferentialUtil.getSpeciesMap())

  private callback: (
    speciesKey: string,
    cityBorn: string | null,
    nativeTongue: string | null,
  ) => void

  constructor(
    object: Model,
    nativeTongues: string[],
    callback: (
      speciesKey: string,
      cityBorn: string | null,
      nativeTongue: string | null,
    ) => void,
    undo?: () => void,
    options?: Partial<FormApplicationOptions>,
  ) {
    super(object, undo, options)
    this.callback = callback
    this.model.nativeTongues = nativeTongues

    // Init species
    this.initSpecies(this.model.data?.speciesKey)
  }

  public static get defaultOptions(): FormApplicationOptions {
    return mergeObject(super.defaultOptions, {
      id: 'creature-pnj-chooser',
      title: i18n().localize('WFRP4NPCGEN.species.select.title'),
      template: `modules/${RegisterSettings.moduleName}/templates/creature-pnj-chooser.html`,
      width: 400,
    })
  }

  public static async selectSpecies(
    initSpeciesKey: string,
    initCityBorn: string | null,
    initNativeTongue: string | null,
    callback: (
      speciesKey: string,
      cityBorn: string | null,
      nativeTongue: string | null,
    ) => void,
    undo?: () => void,
  ) {
    const nativeTongs: string[] = await ChooserUtil.getLanguages()

    new CreaturePnjChooser(
      new Model(initSpeciesKey, initCityBorn, initNativeTongue),
      nativeTongs,
      callback,
      undo,
    ).render(true)
  }

  public activateListeners(html: JQuery) {
    super.activateListeners(html)
    this.handleClick(html, '#randomSpeciesButton', (_event) => {
      const randomSpeciesKey = RandomUtil.getRandomValue(this.speciesKeys)
      this.selectSpecies(randomSpeciesKey)
      this.render()
    })
    this.handleChange(html, '#speciesSelect', (event) => {
      this.selectSpecies(event.currentTarget.value)
      this.render()
    })
    this.handleChange(html, '#cityBornInput', (event) => {
      this.updateCityborn(event.currentTarget.value)
    })
    this.handleChange(html, '#nativeTongueInput', (event) => {
      this.updateNativeTongue(event.currentTarget.value)
    })
  }

  protected isValid(_data): boolean {
    return true
  }

  protected yes(data: Model) {
    this.callback(data?.speciesKey, data?.cityBorn, data?.nativeTongue)
  }

  private initSpecies(speciesKey) {
    const species: SelectModel[] = []
    species.push(
      new SelectModel(
        'none',
        '',
        speciesKey == null || speciesKey.length === 0,
      ),
    )
    const speciesMap = ReferentialUtil.getSpeciesMap()
    this.speciesKeys.forEach((key) => {
      species.push(new SelectModel(key, speciesMap[key], key === speciesKey))
    })
    this.model.species = species
    this.model.data.speciesKey = speciesKey
  }

  private selectSpecies(speciesKey: string) {
    this.model.species = this.model.species.map((spec) => {
      return new SelectModel(spec.key, spec.value, speciesKey === spec.key)
    })
    this.model.data.speciesKey = speciesKey
  }

  private updateCityborn(cityBorn: string | null) {
    this.model.data.cityBorn = cityBorn
  }

  private updateNativeTongue(nativeTongue: string | null) {
    this.model.data.nativeTongue = nativeTongue
  }
}
