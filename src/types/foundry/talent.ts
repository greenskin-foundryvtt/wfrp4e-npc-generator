import { FoundryItem, FoundryItemData } from './item'

export interface FoundryTalent extends FoundryItem<FoundryTalentData> {}

export interface FoundryTalentData extends FoundryItemData {}
