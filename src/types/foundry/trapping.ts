import { FoundryItem, FoundryItemData } from './item'

export interface FoundryTrapping extends FoundryItem<FoundryTrappingData> {}
export interface FoundryTrappingData extends FoundryItemData {}
