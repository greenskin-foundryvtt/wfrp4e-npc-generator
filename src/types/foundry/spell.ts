import { FoundryItem, FoundryItemData } from './item'

export interface FoundrySpell extends FoundryItem<FoundrySpellData> {}
export interface FoundrySpellData extends FoundryItemData {}
