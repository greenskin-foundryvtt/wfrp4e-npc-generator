import { FoundryActor } from './actor'
import { FoundryFolder } from './folder'
import { FoundryItem } from './item'
import { FoundryTable } from './table'
import { FoundrySkill } from './skill'
import { FoundryTrapping } from './trapping'
import { NpcGenerator } from '../../generators/npc/npc-generator'
import { CreatureGenerator } from '../../generators/creature/creature-generator'

declare global {
  const duplicate: <T>(obj: T) => T

  const randomID: (nbr: number) => string

  const Folder: {
    create: (opt: {
      name: string
      type: string
      folder: FoundryFolder | null
    }) => Promise<FoundryFolder>
  }

  const Item: {
    metadata: {
      name: string
    }
  }

  const Token: {
    embeddedName: string
  }

  const Actor: {
    create: (data: FoundryActor) => Promise<FoundryActor>
    get: (id: string | undefined) => FoundryActor
  }

  type FormApplicationOptions = object

  class FormApplication<
    Options extends FormApplicationOptions = FormApplicationOptions,
    Data extends object = FormApplication.Data<{}, Options>,
    ConcreteObject = Data extends FormApplication.Data<infer T, Options>
      ? T
      : {},
  > {
    public static defaultOptions: object
    public object: ConcreteObject
    constructor(object: ConcreteObject, options: Partial<Options>)

    public close(options?: object): Promise<void>
    protected _onSubmit(
      event: Event,
      options?: FormApplication.OnSubmitOptions,
    ): Promise<Partial<Record<string, unknown>>>
    public activateListeners(html: JQuery): void
    public render(opt?: boolean): void
  }

  const mergeObject: (
    obj1: object | undefined,
    obj2: object | undefined,
    opt?: object | undefined,
  ) => object

  const game: FoundryGame
  const ui: FoundryUi
  const loadTemplates: (paths: string[]) => void

  namespace FormApplication {
    class Data<DATA, OPTS> {}
    interface OnSubmitOptions {}
  }

  interface NumberConstructor {
    isNumeric(n: unknown): boolean
  }

  class Roll {
    constructor(roll: string)
    public roll(): Promise<{ total: number }>
  }
}

export interface FoundryGame {
  wfrp4e: FoundryWfrp4e
  user: FoundryUser
  actors: FoundryActor[] & {
    tokens: FoundryActor[]
    get: (id: string) => FoundryActor
  }
  i18n: FoundryI18n
  modules: FoundryModules
  packs: FoundryPack[]
  babele: FoundryBabele
  folders: FoundryFolder[]
  settings: FoundrySettings
  items: FoundryItem[]
  world: FoundryWorld
  tables: FoundryTable[]
}

export interface FoundryUi {
  notifications: FoundryNotifications
}

export interface FoundryWfrp4e {
  speciesReferencial: any
  names: FoundryNameGenWfrp
  config: FoundryWfrp4eConfig
  utility: {
    allBasicSkills: () => Promise<FoundrySkill[]>
    speciesCharacteristics: (
      key: string,
      average: boolean,
    ) => Promise<{
      ag: { value: number; formula: string }
      bs: { value: number; formula: string }
      dex: { value: number; formula: string }
      fel: { value: number; formula: string }
      i: { value: number; formula: string }
      int: { value: number; formula: string }
      s: { value: number; formula: string }
      t: { value: number; formula: string }
      wp: { value: number; formula: string }
      ws: { value: number; formula: string }
    }>
    speciesMovement: (key: string) => Promise<number>
    allMoneyItems: () => Promise<FoundryTrapping[]>
  }
  market: {
    consolidateMoney: (
      items: Record<string, unknown>[],
    ) => Record<string, unknown>[]
  }
  npcGen: NpcGenerator
  creatureGen: CreatureGenerator
}
export interface FoundryWfrp4eConfig {
  trappingCategories: {
    ammunition: string
    armour: string
    booksAndDocuments: string
    clothingAccessories: string
    container: string
    drugsPoisonsHerbsDraughts: string
    foodAndDrink: string
    ingredient: string
    misc: string
    money: string
    toolsAndKits: string
    tradeTools: string
    weapon: string
  }
  actorSizes: {
    avg: string
    enor: string
    lrg: string
    ltl: string
    mnst: string
    sml: string
    tiny: string
  }
  weaponGroups: {
    basic: string
    blackpowder: string
    bow: string
    brawling: string
    catapult: string
    cavalry: string
    crossbow: string
    engineering: string
    entangling: string
    explosives: string
    fencing: string
    flail: string
    parry: string
    polearm: string
    sling: string
    throwing: string
    twohanded: string
    vehicle: string
    warMachine: string
  }
  classTrappings: {
    [key: string]: string
  }
  species: {
    [key: string]: string
  }
  magicLores: {
    [key: string]: string
  }
  subspecies: FoundryWfrp4eConfigSubSpecies
  statusTiers: {
    b: string
    s: string
    g: string
  }
  speciesSkills: { [key: string]: string[] }
  speciesTalents: { [key: string]: (string | number)[] }
  prayerTypes: {
    blessing: string
    miracle: string
  }
}
export interface FoundryWfrp4eConfigSubSpecies {
  [speciesKey: string]: FoundryWfrp4eConfigSubSpeciesDetail
}
export interface FoundryWfrp4eConfigSubSpeciesDetail {
  [subSpeciesKey: string]: {
    name: string
    skills?: string[]
    talents?: (string | number)[]
  }
}
export interface FoundryUser {
  isGM: boolean
  can: (role: string) => boolean
}
export interface FoundryI18n {
  localize: (label: string) => string
  format: (key: string, args: { [key: string]: string }) => string
  translations: FoundryTranslations
}
export interface FoundryTranslations {
  [key: string]: string | FoundryTranslations
}
export interface FoundryModules {
  get: (key: string) => { active: boolean }
}
export interface FoundryPack {
  documentName: string
  getDocuments: <T>() => Promise<T>
  title: string
  collection: string
  metadata: {
    id: string
  }
}
export interface FoundryBabele {
  modules: []
}
export interface FoundrySettings {
  get: <T = string>(moduleName: string, settingName: string) => T
  set: (
    moduleName: string,
    settingName: string,
    setting: { [key: string]: any },
  ) => Promise<void>
  register: (
    moduleName: string,
    name: string,
    opt: {
      name: string
      hint: string
      scope: string
      config?: boolean
      type:
        | BooleanConstructor
        | StringConstructor
        | NumberConstructor
        | ObjectConstructor
      default: boolean | string | number | object
      choices?: {
        [key: string]: string
      }
      onChange?: (s: unknown) => void
    },
  ) => void
  registerMenu: (
    moduleName: string,
    settingName: string,
    opt: {
      name: string
      label: string
      hint: string
      icon: string
      restricted: boolean
      type: object
    },
  ) => void
}
export interface FoundryWorld {
  title: string
}
export interface FoundryNotifications {
  id: string
  info: (msg: string, options?: object) => void
  warn: (msg: string, options?: object) => void
  error: (msg: string, options?: object) => void
  clear: () => void
  rendered: boolean
}
export interface FoundryNameGenWfrp {
  generateName: (opt: { species: string }) => string
}
