declare class Hooks {
  static on<K extends keyof Hooks.StaticCallbacks>(
    hook: K,
    fn: Hooks.StaticCallbacks[K],
  ): number
  static on<H extends Hooks.DynamicCallbacks>(hook: string, fn: H): number
  static on<H extends (...args: any) => any>(hook: string, fn: H): number

  static once<K extends keyof Hooks.StaticCallbacks>(
    hook: K,
    fn: Hooks.StaticCallbacks[K],
  ): ReturnType<(typeof Hooks)['on']>
  static once<H extends Hooks.DynamicCallbacks>(
    hook: string,
    fn: H,
  ): ReturnType<(typeof Hooks)['on']>
  static once<H extends (...args: any) => any>(
    hook: string,
    fn: H,
  ): ReturnType<(typeof Hooks)['on']>
}

declare namespace Hooks {
  interface StaticCallbacks {}

  type DynamicCallbacks = object
}
