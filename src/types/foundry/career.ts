import { FoundryItem, FoundryItemData } from './item'

export interface FoundryCareer extends FoundryItem<FoundryCareerData> {}

export interface FoundryCareerData extends FoundryItemData {}
