declare interface DialogOptions {}

interface DialogRenderOption {}

declare class Dialog<Options extends DialogOptions = DialogOptions> {
  constructor(data: Dialog.Data, options?: Partial<Options>)

  render(force?: boolean, options?: DialogRenderOption): void

  close(): Promise<void>
}

declare namespace Dialog {
  interface Data {}
}
