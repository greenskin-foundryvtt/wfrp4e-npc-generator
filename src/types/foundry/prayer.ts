import { FoundryItem, FoundryItemData } from './item'

export interface FoundryPrayer extends FoundryItem<FoundryPrayerData> {}
export interface FoundryPrayerData extends FoundryItemData {}
