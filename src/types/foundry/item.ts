export interface FoundryItem<T extends FoundryItemData = FoundryItemData>
  extends Record<string, unknown> {
  id: string
  _id?: string
  name: string
  displayName: string
  DisplayName: string
  type: string
  system: T
  flags?: {
    babele?: {
      originalName: string
      hasTranslation: boolean
    }
  }
  document: {
    pack: string
  }
  pack: string
  toObject: () => Record<string, unknown>
  img: string
}
export interface FoundryItemData {
  trappingType: {
    value: string
  }
  mutationType: {
    value: string
  }
  specification: {
    value: string | number
  }
  advanced: {
    value: string
  }
  advances: {
    value: number
  }
  quantity: {
    value: number
  }
  careergroup?: {
    value?: string
  }
  level?: {
    value?: string
  }
  current?: {
    value?: boolean
  }
  complete?: {
    value?: boolean
  }
  class?: {
    value?: string
  }
  lore: {
    value: string
  }
  type: {
    value: string
  }
  god: {
    value: string
  }
  weaponGroup: {
    value: string
  }
  ammunitionGroup: {
    value: string
  }
  ammunitionType: {
    value: string
  }
  coinValue: {
    value: number
  }
  status: {
    standing: number
    tier: string
  }
  skills: string[]
  talents: string[]
  characteristics: {
    [key: string]: {
      advances: number
      bonus: number
      bonusMod: number
      calculationBonusModifier: number
      cost: number
      initial: number
      key: string
      modifier: number
      value: number
    }
  }
  trappings: string[]
  disabled?: boolean
}
