import StringUtil from './string-util'
import deburr from './lodash/deburr'
import RandomUtil from './random-util'
import { babele, i18n } from '../constant'
import { SourceSelectModel } from '../models/common/source-select-model'
import { FoundryItem } from '../types/foundry/item'
import { FoundryActor } from '../types/foundry/actor'

export default class EntityUtil {
  public static searchCache: { [name: string]: FoundryItem } = {}

  public static particularCase: { [name: string]: () => string } = {
    warleader: () => i18n().localize('WFRP4NPCGEN.TALENT.WarLeader'),
  }

  public static match(
    item: { name: string },
    ref: FoundryItem | null,
  ): boolean {
    return (
      StringUtil.getSimpleName(item.name) ===
        StringUtil.getSimpleName(ref?.name) ||
      StringUtil.getSimpleName(item.name) ===
        StringUtil.getSimpleName(ref?.flags?.babele?.originalName)
    )
  }

  public static toSelectOption(items: FoundryItem[]): {
    [key: string]: string
  } {
    if (items == null) {
      return {}
    }
    const map: { [key: string]: string } = {}
    for (let item of items) {
      if (map[item._id as string] == null) {
        try {
          map[item._id as string] =
            item.displayName ?? item.DisplayName ?? item.name
        } catch (e) {
          map[item._id as string] = item.displayName ?? item.name
        }
      }
    }
    return map
  }

  public static toSourceSelectModel(items: FoundryItem[]): SourceSelectModel[] {
    if (items == null) {
      return []
    }
    const models: SourceSelectModel[] = []
    const ids: string[] = []
    for (let item of items) {
      if (!ids.includes(item._id as string)) {
        ids.push(item._id as string)
        models.push(this.toSingleSourceSelectModel(item))
      }
    }
    return models
  }

  public static toSingleSourceSelectModel(
    item: FoundryItem,
  ): SourceSelectModel {
    try {
      return new SourceSelectModel(
        item._id as string,
        item.displayName ?? item.DisplayName ?? item.name,
        false,
        item,
      )
    } catch (e) {
      return new SourceSelectModel(
        item._id as string,
        item.displayName ?? item.name,
        false,
        item,
      )
    }
  }

  public static toMinimalName(value: string): string {
    let result = deburr(value).toLowerCase().trim()
    result = result.replace(/\s/g, '')
    result = result.replace(/\(/g, '')
    result = result.replace(/\)/g, '')
    return result
  }

  public static find<T extends FoundryItem = FoundryItem>(
    searchName: string,
    entities: T[],
  ): T | null {
    let name = searchName
    if (name == null || entities?.length <= 0) {
      return null
    }
    const cacheKey = `${searchName}-${entities[0].type}`
    if (this.searchCache[cacheKey] != null) {
      return this.searchCache[cacheKey] as T
    }
    const i18nKeyRegexp = /(WFRP4NPCGEN(?:\.[\w]+)*)/g
    const matches = i18nKeyRegexp.exec(name) ?? []
    if (matches.length > 1) {
      const key = matches[1] ?? null
      name = key != null ? name.replace(key, i18n().localize(key)) : name
    }
    const matchName = StringUtil.toDeburrLowerCase(name).trim()
    let findByVo = false
    let findByVoExactMatch = false
    const hasBabele = babele()?.modules?.length > 0 ?? false

    // Manage particular case with *
    if (name.includes('*')) {
      const normalised = this.find(name.replace('*', ''), entities)
      if (normalised != null) {
        return normalised
      }
    }

    // Manage particular case with open ( but no close )
    if (name.includes('(') && !name.includes(')')) {
      const normalised = this.find(`${name})`, entities)
      if (normalised != null) {
        return normalised
      }
    }

    // Manage syntaxe error
    if (this.isParticularCase(name)) {
      const normalised = this.find(this.getParticularCase(name), entities)
      if (normalised != null) {
        return normalised
      }
    }

    let result = entities.find(
      (e: FoundryItem) =>
        (!hasBabele || e.flags?.babele?.hasTranslation) &&
        StringUtil.equalsDeburrIgnoreCase(e.name.trim(), matchName),
    )
    if (result == null) {
      result = entities.find((e: FoundryItem) =>
        StringUtil.equalsDeburrIgnoreCase(
          e.flags?.babele?.originalName?.trim(),
          matchName,
        ),
      )
      findByVo = result != null
      findByVoExactMatch = result != null
    }
    if (result == null) {
      const simpleMatchName = StringUtil.toDeburrLowerCase(
        StringUtil.getSimpleName(name),
      ).trim()
      result = entities.find(
        (e: FoundryItem) =>
          (!hasBabele || e.flags?.babele?.hasTranslation) &&
          StringUtil.equalsDeburrIgnoreCase(
            StringUtil.getSimpleName(e.name).trim(),
            simpleMatchName,
          ),
      )
      if (result == null) {
        result = entities.find((e: FoundryItem) =>
          StringUtil.equalsDeburrIgnoreCase(
            StringUtil.getSimpleName(e.flags?.babele?.originalName)?.trim(),
            simpleMatchName,
          ),
        )
        findByVo = result != null
      }
    }
    if (result == null) {
      result = entities.find(
        (e: FoundryItem) =>
          hasBabele &&
          !e.flags?.babele?.hasTranslation &&
          StringUtil.equalsDeburrIgnoreCase(e.name.trim(), matchName),
      )
      findByVo = result != null
      findByVoExactMatch = result != null
    }
    if (result == null) {
      const simpleMatchName = StringUtil.toDeburrLowerCase(
        StringUtil.getSimpleName(name),
      ).trim()
      result = entities.find(
        (e: FoundryItem) =>
          hasBabele &&
          !e.flags?.babele?.hasTranslation &&
          StringUtil.equalsDeburrIgnoreCase(
            StringUtil.getSimpleName(e.name).trim(),
            simpleMatchName,
          ),
      )
      findByVo = result != null
    }
    if (result != null) {
      const data = duplicate(result)
      const originalName = result.flags?.babele?.originalName?.trim()
      const originalSimpleName =
        originalName != null && name.includes('(')
          ? StringUtil.getSimpleName(originalName)
          : null
      data._id = RandomUtil.getRandomId()
      data.pack = result.pack
      const simpleVoSameHasTranslate =
        originalSimpleName != null &&
        StringUtil.equalsDeburrIgnoreCase(
          originalSimpleName,
          StringUtil.getSimpleName(data.name),
        )
      if (findByVo || simpleVoSameHasTranslate) {
        if (!findByVoExactMatch && name.includes('(')) {
          const tradSimpleName = StringUtil.getSimpleName(data.name).trim()
          let groupName = StringUtil.getGroupName(name).trim()
          data.name = `${tradSimpleName} (${i18n().localize(groupName)})`
        }
      } else {
        data.name = name
      }
      this.searchCache[cacheKey] = data
      return data
    }
    return null
  }

  public static hasGroupName(name: string): boolean {
    return name?.includes('(') && name?.includes(')')
  }

  public static hasGroup(item: { name: string }): boolean {
    return this.hasGroupName(item?.name)
  }

  public static hasSpec(item: FoundryItem): boolean {
    const spec = item?.system?.specification?.value as string
    return spec?.length > 0
  }

  public static isWorldItem(item: FoundryItem | FoundryActor): boolean {
    return item?.pack == null
  }

  public static toRecords(arrays: FoundryItem[]): Record<string, unknown>[] {
    return arrays.map((d) => (d.toObject != null ? d.toObject() : d))
  }

  public static toDuplicateRecords(
    arrays: FoundryItem[],
  ): Record<string, unknown>[] {
    return arrays.map((d) => {
      const record = duplicate(d.toObject != null ? d.toObject() : d)
      delete record._id
      return record
    })
  }

  public static duplicateWithNewId<E>(
    entity: E & { _id?: string | undefined },
  ): E {
    const nextEntity = duplicate(entity)
    nextEntity._id = RandomUtil.getRandomId()
    return <E>nextEntity
  }

  public static duplicateWithNewIds<E>(
    entities: (E & { _id?: string | undefined })[],
  ): E[] {
    return entities?.map((e) => this.duplicateWithNewId(e)) ?? []
  }

  private static isParticularCase(name: string): boolean {
    return (
      name != null &&
      this.particularCase[StringUtil.toDeburrLowerCase(name)] != null
    )
  }

  private static getParticularCase(name: string): string {
    return name != null
      ? this.particularCase[StringUtil.toDeburrLowerCase(name)]()
      : ''
  }
}
