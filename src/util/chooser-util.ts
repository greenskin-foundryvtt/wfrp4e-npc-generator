import { SourceSelectModel } from '../models/common/source-select-model'
import ReferentialUtil from './referential-util'
import StringUtil from './string-util'
import RandomUtil from './random-util'
import EntityUtil from './entity-util'
import { GroupedSourceSelectModel } from '../models/common/grouped-source-select-model'
import { i18n, wfrp4e } from '../constant'
import CompendiumUtil from './compendium-util'
import { FoundrySkill } from '../types/foundry/skill'
import { FoundryTalent } from '../types/foundry/talent'
import { FoundryTrait } from '../types/foundry/trait'

export default class ChooserUtil {
  private static readonly trappingsSortList = [
    'weapon',
    'ammunition',
    'armour',
    'container',
    'clothingAccessories',
    'foodAndDrink',
    'toolsAndKits',
    'booksAndDocuments',
    'tradeTools',
    'drugsPoisonsHerbsDraughts',
    'misc',
    'ingredient',
    'money',
    'others',
  ]

  private static readonly trappingsCategorieEntries = () =>
    Object.entries(
      <{ [key: string]: string }>wfrp4e().config.trappingCategories,
    )

  private static readonly trappingsSort: (
    a: GroupedSourceSelectModel,
    b: GroupedSourceSelectModel,
  ) => number = (a: GroupedSourceSelectModel, b: GroupedSourceSelectModel) => {
    const entry1 = ChooserUtil.trappingsCategorieEntries()?.find(
      ([_key, value]) => value === a.group,
    )
    const entry2 = ChooserUtil.trappingsCategorieEntries()?.find(
      ([_key, value]) => value === b.group,
    )
    const key1 = entry1 != null ? entry1[0] : 'others'
    const key2 = entry2 != null ? entry2[0] : 'others'

    return (
      ChooserUtil.trappingsSortList.indexOf(key1) -
      ChooserUtil.trappingsSortList.indexOf(key2)
    )
  }

  private static readonly spellsLoreEntries = () => [
    ...Object.entries(<{ [key: string]: string }>wfrp4e().config.magicLores),
    ['undivided', 'Chaos'],
    ['warp', 'Skaven'],
    ['none', i18n().localize('WFRP4NPCGEN.select.magics.no.lore.label')],
  ]

  private static readonly spellsLoreSortList = [
    'petty',
    'none',
    'beasts',
    'heavens',
    'fire',
    'light',
    'metal',
    'death',
    'shadow',
    'life',
    'hedgecraft',
    'witchcraft',
    'daemonology',
    'necromancy',
    'undivided',
    'nurgle',
    'slaanesh',
    'tzeentch',
    'warp',
    'plague',
    'stealth',
    'ruin',
    'elementalism',
    'seasons',
    'druid',
    "lil' waaagh!",
    "da lit'lest waaagh!",
    'da big waaagh!',
    'others',
  ]

  private static readonly spellsSort = (
    a: GroupedSourceSelectModel,
    b: GroupedSourceSelectModel,
  ) => {
    const entry1 = ChooserUtil.spellsLoreEntries()?.find(
      ([_key, value]) => value === a.group,
    )
    const entry2 = ChooserUtil.spellsLoreEntries()?.find(
      ([_key, value]) => value === b.group,
    )
    const key1 = entry1 != null ? entry1[0] : 'others'
    const key2 = entry2 != null ? entry2[0] : 'others'

    return (
      ChooserUtil.spellsLoreSortList.indexOf(key1) -
      ChooserUtil.spellsLoreSortList.indexOf(key2)
    )
  }

  private static readonly prayersGodSortList = () => [
    `${wfrp4e().config.prayerTypes.blessing}`,
    `${wfrp4e().config.prayerTypes.miracle} - ${i18n().localize('WFRP4NPCGEN.Manann')}`,
    `${wfrp4e().config.prayerTypes.miracle} - ${i18n().localize('WFRP4NPCGEN.Morr')}`,
    `${wfrp4e().config.prayerTypes.miracle} - ${i18n().localize('WFRP4NPCGEN.Myrmidia')}`,
    `${wfrp4e().config.prayerTypes.miracle} - ${i18n().localize('WFRP4NPCGEN.Ranald')}`,
    `${wfrp4e().config.prayerTypes.miracle} - ${i18n().localize('WFRP4NPCGEN.Rhya')}`,
    `${wfrp4e().config.prayerTypes.miracle} - ${i18n().localize('WFRP4NPCGEN.Shallya')}`,
    `${wfrp4e().config.prayerTypes.miracle} - ${i18n().localize('WFRP4NPCGEN.Sigmar')}`,
    `${wfrp4e().config.prayerTypes.miracle} - ${i18n().localize('WFRP4NPCGEN.Taal')}`,
    `${wfrp4e().config.prayerTypes.miracle} - ${i18n().localize('WFRP4NPCGEN.Ulric')}`,
    `${wfrp4e().config.prayerTypes.miracle} - ${i18n().localize('WFRP4NPCGEN.Verena')}`,
  ]

  private static readonly prayersSort = (
    a: GroupedSourceSelectModel,
    b: GroupedSourceSelectModel,
  ) => {
    return (
      ChooserUtil.prayersGodSortList().indexOf(a.group) -
      ChooserUtil.prayersGodSortList().indexOf(b.group)
    )
  }

  private static readonly mutationsSortList = () => [
    'Undivided',
    'Khorne',
    'Nurgle',
    'Slaanesh',
    'Tzeentch',
  ]

  private static readonly mutationsSort = (a: string, b: string) => {
    return (
      ChooserUtil.mutationsSortList().indexOf(a) -
      ChooserUtil.mutationsSortList().indexOf(b)
    )
  }

  public static readonly correctDataName = (
    data: object,
    key: string = 'name',
  ) => {
    if (data[key] == null) {
      return
    }
    if (data[key].includes('*')) {
      data[key] = data[key].replace(/\*/g, '')
    }
    if (data[key].includes('(') && !data[key].includes(')')) {
      data[key] = data[key] + ')'
    }
  }

  public static async getEditSkills(
    initSkills: FoundrySkill[],
  ): Promise<SourceSelectModel[]> {
    const initSkillsNames = initSkills.map((s) => s.name)
    const skills = [
      ...initSkills.sort((s1, s2) => {
        return s1.name.localeCompare(s2.name)
      }),
      ...[
        ...(await ReferentialUtil.getSkillEntities(true))
          .filter((s) => {
            return !StringUtil.arrayIncludesDeburrIgnoreCase(
              initSkillsNames,
              s.name ?? '',
            )
          })
          .map((s) => {
            const data = duplicate(s)
            data._id = RandomUtil.getRandomId()
            data.pack = s.pack
            return data
          }),
        ...(await ReferentialUtil.getCompendiumActorSkills())
          .filter((s) => {
            return !StringUtil.arrayIncludesDeburrIgnoreCase(
              initSkillsNames,
              s.name,
            )
          })
          .map((s) => {
            const data = duplicate(s)
            this.correctDataName(data)
            data._id = RandomUtil.getRandomId()
            data.pack = s.pack
            return data
          }),
      ].sort((s1, s2) => {
        return s1.name.localeCompare(s2.name)
      }),
    ]

    return Promise.resolve(EntityUtil.toSourceSelectModel(skills))
  }

  public static async getEditTalents(
    initTalents: FoundryTalent[],
  ): Promise<SourceSelectModel[]> {
    const initTalentsNames = initTalents.map((t) => t.name)
    const talents = [
      ...initTalents.sort((t1, t2) => {
        return t1.name.localeCompare(t2.name)
      }),
      ...[
        ...(await ReferentialUtil.getTalentEntities(true))
          .filter((t) => {
            return !StringUtil.arrayIncludesDeburrIgnoreCase(
              initTalentsNames,
              t.name ?? '',
            )
          })
          .map((t) => {
            const data = duplicate(t)
            data._id = RandomUtil.getRandomId()
            data.pack = t.pack
            return data
          }),
        ...(await ReferentialUtil.getCompendiumActorTalents())
          .filter((t) => {
            return !StringUtil.arrayIncludesDeburrIgnoreCase(
              initTalentsNames,
              t.name,
            )
          })
          .map((t) => {
            const data = duplicate(t)
            this.correctDataName(data)
            data._id = RandomUtil.getRandomId()
            data.pack = t.pack
            return data
          }),
      ].sort((t1, t2) => {
        return t1.name.localeCompare(t2.name)
      }),
    ]

    return Promise.resolve(EntityUtil.toSourceSelectModel(talents))
  }

  public static async getEditTraits(
    initTraits: FoundryTrait[],
    forCreature = false,
  ): Promise<SourceSelectModel[]> {
    const swarm = forCreature
      ? await CompendiumUtil.getCompendiumSwarmTrait()
      : null
    const weapon = forCreature
      ? await CompendiumUtil.getCompendiumWeaponTrait()
      : null
    const armour = forCreature
      ? await CompendiumUtil.getCompendiumArmourTrait()
      : null
    const ranged = forCreature
      ? await CompendiumUtil.getCompendiumRangedTrait()
      : null
    const size = forCreature
      ? await CompendiumUtil.getCompendiumSizeTrait()
      : null
    const initTraitsNames = initTraits.map((t) => t.name)
    const initTraitsDisplayNames = initTraits.map((t) => t.DisplayName)
    const traits = [
      ...initTraits.sort((t1, t2) => {
        return (t1.DisplayName ?? t1.name).localeCompare(
          t2.DisplayName ?? t2.name,
        )
      }),
      ...[
        ...(await ReferentialUtil.getTraitEntities(true))
          .filter((t) => {
            const excludeCreatureTraitCheck = forCreature
              ? !EntityUtil.match(t, swarm) &&
                !EntityUtil.match(t, weapon) &&
                !EntityUtil.match(t, armour) &&
                !EntityUtil.match(t, ranged) &&
                !EntityUtil.match(t, size)
              : true
            const specification = t?.system?.specification?.value as string
            return (
              excludeCreatureTraitCheck &&
              (!StringUtil.arrayIncludesDeburrIgnoreCase(
                initTraitsNames,
                t.name ?? '',
              ) ||
                specification?.length > 0)
            )
          })
          .map((t) => {
            const data = duplicate(t)
            data._id = RandomUtil.getRandomId()
            data.pack = t.pack
            return data
          }),
        ...(await ReferentialUtil.getCompendiumActorTraits())
          .filter((t) => {
            const excludeCreatureTraitCheck = forCreature
              ? !EntityUtil.match(t, swarm) &&
                !EntityUtil.match(t, weapon) &&
                !EntityUtil.match(t, armour) &&
                !EntityUtil.match(t, ranged) &&
                !EntityUtil.match(t, size)
              : true
            return (
              excludeCreatureTraitCheck &&
              !StringUtil.arrayIncludesDeburrIgnoreCase(
                initTraitsDisplayNames,
                t.DisplayName,
              )
            )
          })
          .map((t) => {
            const data = duplicate(t)
            this.correctDataName(data)
            this.correctDataName(data, 'DisplayName')
            data._id = RandomUtil.getRandomId()
            data.pack = t.pack
            return data
          }),
      ].sort((t1, t2) => {
        return t1.name.localeCompare(t2.name)
      }),
    ]

    return Promise.resolve(EntityUtil.toSourceSelectModel(traits))
  }

  public static async getEditTrappings(): Promise<GroupedSourceSelectModel[]> {
    const trappings = [
      ...(await ReferentialUtil.getTrappingEntities(true)),
    ].sort((t1, t2) => {
      return t1.name.localeCompare(t2.name)
    })

    const trappingsMap: GroupedSourceSelectModel[] = []
    for (let trapping of trappings) {
      const type = trapping?.system?.trappingType?.value ?? trapping.type
      const categorie = wfrp4e().config.trappingCategories[type]
      let groupedModel = trappingsMap.find((m) => m.group === categorie)
      if (groupedModel == null) {
        groupedModel = new GroupedSourceSelectModel(categorie)
        trappingsMap.push(groupedModel)
      }

      groupedModel.items.push(EntityUtil.toSingleSourceSelectModel(trapping))
    }

    return Promise.resolve(trappingsMap.sort(this.trappingsSort))
  }

  public static async getEditSpells(): Promise<GroupedSourceSelectModel[]> {
    const spells = [...(await ReferentialUtil.getSpellEntities(true))].sort(
      (t1, t2) => {
        return t1.name.localeCompare(t2.name)
      },
    )

    const spellsMap: GroupedSourceSelectModel[] = []
    for (let spell of spells) {
      const lore = StringUtil.toDeburrLowerCase(
        spell?.system?.lore?.value ?? '',
      )
      const loreLabel =
        lore == null || lore?.trim() == ''
          ? i18n().localize('WFRP4NPCGEN.select.magics.no.lore.label')
          : wfrp4e().config.magicLores[lore] ??
            wfrp4e().config.magicLores[this.getCorrectedLore(lore)] ??
            this.getCorrectedLore(lore)

      let groupedSpells = spellsMap.find((gs) => gs.group === loreLabel)

      if (groupedSpells == null) {
        groupedSpells = new GroupedSourceSelectModel(loreLabel)
        spellsMap.push(groupedSpells)
      }
      groupedSpells.items.push(EntityUtil.toSingleSourceSelectModel(spell))
    }

    return Promise.resolve(spellsMap.sort(this.spellsSort))
  }

  public static async getEditPrayers(): Promise<GroupedSourceSelectModel[]> {
    const prayers = [...(await ReferentialUtil.getPrayerEntities(true))].sort(
      (t1, t2) => {
        return t1.name.localeCompare(t2.name)
      },
    )

    const prayersMap: GroupedSourceSelectModel[] = []
    for (let prayer of prayers) {
      const type = prayer?.system?.type?.value
      const god = prayer?.system?.god?.value
      const key =
        type === 'blessing'
          ? wfrp4e().config.prayerTypes[type]
          : `${wfrp4e().config.prayerTypes[type]} - ${god}`

      let groupedPrayers = prayersMap.find((gs) => gs.group === key)

      if (groupedPrayers == null) {
        groupedPrayers = new GroupedSourceSelectModel(key)
        prayersMap.push(groupedPrayers)
      }
      groupedPrayers.items.push(EntityUtil.toSingleSourceSelectModel(prayer))
    }

    return Promise.resolve(prayersMap.sort(this.prayersSort))
  }

  public static async getEditPhysicalMutations(): Promise<
    GroupedSourceSelectModel[]
  > {
    const physicals = await ReferentialUtil.getPhysicalMutationMaps()

    const grouped: GroupedSourceSelectModel[] = []
    for (let key of Object.keys(physicals).sort(this.mutationsSort)) {
      const group = new GroupedSourceSelectModel(i18n().localize(key))
      group.items = EntityUtil.toSourceSelectModel(physicals[key])
      grouped.push(group)
    }

    return Promise.resolve(grouped)
  }

  public static async getEditMentalMutations(): Promise<
    GroupedSourceSelectModel[]
  > {
    const mentals = await ReferentialUtil.getMentalMutationMaps()

    const grouped: GroupedSourceSelectModel[] = []
    for (let key of Object.keys(mentals).sort(this.mutationsSort)) {
      const group = new GroupedSourceSelectModel(i18n().localize(key))
      group.items = EntityUtil.toSourceSelectModel(mentals[key])
      grouped.push(group)
    }

    return Promise.resolve(grouped)
  }

  private static readonly moduleOrder = [
    'wfrp4e-core.actors',
    'wfrp4e-starter-set.actors',
    'wfrp4e-archives1.actors',
    'wfrp4e-archives2.actors',
    'wfrp4e-owb1.actors',
    'wfrp4e-ua1.actors',
    'wfrp4e-ua2.actors',
    'wfrp4e-altdorf.actors',
    'wfrp4e-middenheim.actors',
    'wfrp4e-zoo.actors',
    'wfrp4e-up-in-arms.actors',
    'wfrp4e-wom.actors',
    'wfrp4e-rnhd.actors',
    'wfrp4e-eis.actors',
    'wfrp4e-dotr.actors',
    'wfrp4e-pbtt.actors',
    'wfrp4e-horned-rat.actors',
    'wfrp4e-empire-ruins.actors',
    'wfrp4e-unofficial-grimoire.ug-creatures',
    'others',
  ]

  private static readonly creatureSort = (
    a: GroupedSourceSelectModel,
    b: GroupedSourceSelectModel,
  ) => {
    const g1 = a?.group
    const g2 = b?.group
    const key1 = ChooserUtil.moduleOrder.includes(g1) ? g1 : 'others'
    const key2 = ChooserUtil.moduleOrder.includes(g2) ? g2 : 'others'

    return (
      ChooserUtil.moduleOrder.indexOf(key1) -
      ChooserUtil.moduleOrder.indexOf(key2)
    )
  }

  public static async getCreatures(): Promise<GroupedSourceSelectModel[]> {
    const creatures = await ReferentialUtil.getBestiaryEntities()
    const creaturesMap: GroupedSourceSelectModel[] = []
    for (let [groupeKey, actors] of Object.entries(creatures)) {
      const groupedCreatures = new GroupedSourceSelectModel(groupeKey)
      creaturesMap.push(groupedCreatures)
      for (let actor of actors) {
        groupedCreatures.items.push(
          new SourceSelectModel(
            RandomUtil.getRandomId(),
            actor.name ?? '',
            false,
            actor,
          ),
        )
      }
    }
    return Promise.resolve(creaturesMap.sort(ChooserUtil.creatureSort))
  }

  public static async getLanguages(): Promise<string[]> {
    const entities = await ReferentialUtil.getSkillEntities(true)
    const simpleMatchName = i18n().localize('WFRP4NPCGEN.SKILL.Language')
    const result = entities
      .filter(
        (e) =>
          StringUtil.equalsDeburrIgnoreCase(
            StringUtil.getSimpleName(e.name).trim(),
            simpleMatchName,
          ) ||
          StringUtil.equalsDeburrIgnoreCase(
            StringUtil.getSimpleName(e.flags?.babele?.originalName)?.trim(),
            simpleMatchName,
          ),
      )
      .filter((s) => EntityUtil.hasGroup(s))
      .map((s) => StringUtil.getGroupName(s.name))

    return Promise.resolve(result)
  }

  private static getCorrectedLore(lore: string): string {
    switch (lore) {
      case 'minor':
        return 'petty'
      case 'shadows':
        return 'shadow'
      case 'undivided':
        return 'Chaos'
      case 'warp':
        return 'Skaven'
      default:
        return lore
    }
  }
}
