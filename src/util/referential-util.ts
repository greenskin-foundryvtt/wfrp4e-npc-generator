import StringUtil from './string-util'
import CompendiumUtil from './compendium-util'
import EntityUtil from './entity-util'
import { actors, i18n, items, wfrp4e, world } from '../constant'
import {
  origin,
  psychologyPrefix,
  randomTalent,
  traitPrefix,
  speciesReferential,
} from '../referential/species-referential'
import WaiterUtil from './waiter-util'
import RandomUtil from './random-util'
import RegisterSettings from './register-settings'
import { FoundryCareer } from '../types/foundry/career'
import { FoundryMutation } from '../types/foundry/mutation'
import {
  FoundryWfrp4eConfigSubSpecies,
  FoundryWfrp4eConfigSubSpeciesDetail,
} from '../types/foundry/game'
import { FoundryItem } from '../types/foundry/item'
import { FoundryActor } from '../types/foundry/actor'
import { FoundryTrapping, FoundryTrappingData } from '../types/foundry/trapping'
import { FoundrySkill } from '../types/foundry/skill'
import { FoundryTalent } from '../types/foundry/talent'
import { FoundryPsychology } from '../types/foundry/psychology'
import { FoundryTrait } from '../types/foundry/trait'
import { FoundrySpell } from '../types/foundry/spell'
import { FoundryPrayer } from '../types/foundry/prayer'
import { FoundryTable } from '../types/foundry/table'

export default class ReferentialUtil {
  public static readonly mutationsIcons = () => {
    return {
      Undivided: `modules/wfrp4e-core/icons/mutations/mutation.png`,
      Khorne: `modules/${RegisterSettings.moduleName}/assets/Khorne.webp`,
      Nurgle: `modules/${RegisterSettings.moduleName}/assets/Nurgle.webp`,
      Slaanesh: `modules/${RegisterSettings.moduleName}/assets/Slaanesh.webp`,
      Tzeentch: `modules/${RegisterSettings.moduleName}/assets/Tzeentch.webp`,
    }
  }

  public static readonly sortedSize = [
    'tiny',
    'ltl',
    'sml',
    'avg',
    'lrg',
    'enor',
    'mnst',
  ]

  private static speciesSkillsMap: { [key: string]: string[] }
  private static speciesOthersMap: { [key: string]: (string | number)[] }
  private static speciesOriginsMap: {
    [key: string]: {
      [origin: string]: (string | number)[]
    }
  }

  private static subSpeciesSkillsMap: {
    [key: string]: {
      [subKey: string]: string[]
    }
  }
  private static subSpeciesOthersMap: {
    [key: string]: {
      [subKey: string]: (string | number)[]
    }
  }
  private static subSpeciesOriginsMap: {
    [key: string]: {
      [subKey: string]: {
        [origin: string]: (string | number)[]
      }
    }
  }

  private static physicalMutationMaps: { [key: string]: FoundryMutation[] }
  private static mentalMutationMaps: { [key: string]: FoundryMutation[] }

  private static randomTalents: string[]

  private static referentialLoaded = false
  private static creatureReferentialLoaded = false

  public static async initReferential(
    callback: () => void,
    forCreatures = false,
  ) {
    await CompendiumUtil.initCompendium(async () => {
      if (!this.referentialLoaded || !this.creatureReferentialLoaded) {
        await WaiterUtil.show(
          'WFRP4NPCGEN.compendium.load.title',
          'WFRP4NPCGEN.compendium.load.hint',
          async () => {
            if (forCreatures) {
            } else {
              await Promise.all([
                this.getSpeciesSkillsMap(),
                this.getSpeciesOthersMap(),
                this.getRandomTalents(),
              ])
              await Promise.all([
                this.getSubSpeciesSkillsMap(),
                this.getSubSpeciesOthersMap(),
                this.getSpeciesOriginsMap(),
                this.getSubSpeciesOriginsMap(),
              ])
            }

            if (!this.referentialLoaded || !this.creatureReferentialLoaded) {
              await WaiterUtil.hide()
            }
            if (forCreatures) {
              this.creatureReferentialLoaded = true
            } else {
              this.referentialLoaded = true
            }

            callback()
          },
        )
      } else {
        callback()
      }
    }, forCreatures)
  }

  public static getClassTrappings(): { [key: string]: string } {
    const voClassTraping: { [key: string]: string } =
      wfrp4e().config.classTrappings
    const resolvedClassTrapping: { [key: string]: string } = {}
    Object.entries(voClassTraping).forEach(([key]) => {
      let useKey = key
      if (i18n().translations[useKey] == null && useKey.endsWith('s')) {
        useKey = useKey.substring(0, useKey.length - 1)
      }
      const localKey = i18n().localize(useKey)
      resolvedClassTrapping[localKey] = i18n().localize(
        `WFRP4NPCGEN.trappings.class.${key}`,
      )
    })
    return resolvedClassTrapping
  }

  public static getClassKeyFromCareer(career: FoundryCareer) {
    const careerClass = career?.system?.class?.value
    const keys = Object.keys(this.getClassTrappings())
    return keys.find((k) =>
      StringUtil.includesDeburrIgnoreCase(k.trim(), careerClass?.trim()),
    )
  }

  public static getSpeciesMap(): { [key: string]: string } {
    return wfrp4e().config.species
  }

  public static getSubSpeciesMap(): FoundryWfrp4eConfigSubSpecies {
    const result: FoundryWfrp4eConfigSubSpecies = {}
    for (let [key, value] of Object.entries(wfrp4e().config.subspecies)) {
      for (let [subKey, subValue] of Object.entries(value)) {
        const content = {
          name: subValue.name,
        }
        if (result[key] == null) {
          result[key] = {}
        }
        result[key][subKey] = content
      }
    }
    return result
  }

  public static getSpeciesSubSpeciesMap(
    speciesKey: string,
  ): FoundryWfrp4eConfigSubSpeciesDetail | null {
    const subSpecies = this.getSubSpeciesMap()
    return subSpecies != null ? subSpecies[speciesKey] : null
  }

  public static getWeaponTypes(): { melee: string; ranged: string } {
    return {
      melee: i18n().localize('WFRP4NPCGEN.trappings.weapon.skill.melee'),
      ranged: i18n().localize('WFRP4NPCGEN.trappings.weapon.skill.ranged'),
    }
  }

  public static getWeaponGroups(): string[] {
    return Object.values(wfrp4e().config.weaponGroups)
  }

  public static getWeaponGroupsKey(group: string): string {
    for (let key of Object.keys(wfrp4e().config.weaponGroups)) {
      if (
        StringUtil.equalsDeburrIgnoreCase(
          wfrp4e().config.weaponGroups[key],
          group,
        )
      ) {
        return key
      }
    }
    return ''
  }

  public static getMeleeWeaponGroups(): string[] {
    const groups = wfrp4e().config.weaponGroups
    return [
      groups.basic,
      groups.brawling,
      groups.cavalry,
      groups.fencing,
      groups.flail,
      groups.parry,
      groups.polearm,
      groups.twohanded,
    ]
  }

  public static getRangedWeaponGroups(): string[] {
    const groups = wfrp4e().config.weaponGroups
    return [
      groups.blackpowder,
      groups.bow,
      groups.crossbow,
      groups.engineering,
      groups.entangling,
      groups.explosives,
      groups.sling,
      groups.throwing,
    ]
  }

  public static getBasicWeaponGroups(): string {
    return wfrp4e().config.weaponGroups.basic
  }

  public static async getCareerEntities(
    withWorld = true,
  ): Promise<FoundryCareer[]> {
    const careers: FoundryCareer[] = [
      ...(await CompendiumUtil.getCompendiumCareers()),
    ]
    if (withWorld) {
      const worldCareers = await this.getWorldCareers()
      if (worldCareers != null && worldCareers.length > 0) {
        careers.push(...worldCareers)
      }
    }
    return Promise.resolve(careers)
  }

  public static async getCareerEntitieNames(
    withWorld = true,
  ): Promise<string[]> {
    const names = (await this.getCareerEntities(withWorld)).map(
      (c) => c.name ?? '',
    )
    return Promise.resolve(names)
  }

  public static async getWorldCareers(): Promise<FoundryCareer[]> {
    const careersGroups = await CompendiumUtil.getCompendiumCareersGroups()
    const worldCareers = items()
      ?.filter((item: FoundryItem) => {
        return item.type === 'career'
      })
      .filter((item: FoundryCareer) => {
        const group = item?.system?.careergroup?.value
        return !careersGroups.includes(group as string)
      })
    return Promise.resolve(worldCareers)
  }

  public static async getWorldEntities<T extends FoundryItem = FoundryItem>(
    type: string,
  ): Promise<T[]> {
    const worldEntities = items<T>()?.filter((item: T) => {
      return item.type === type
    })
    return Promise.resolve(worldEntities)
  }

  public static async getWorldActorEntities(
    type?: string,
  ): Promise<FoundryActor[]> {
    const worldEntities = actors()?.filter((actor: FoundryActor) => {
      return type != null ? actor.type === type : true
    })
    return Promise.resolve(worldEntities)
  }

  public static async getTrappingEntities(
    withWorld = true,
  ): Promise<FoundryTrapping[]> {
    const trappings: FoundryTrapping[] =
      await CompendiumUtil.getCompendiumTrappings()
    const finalTrappings: FoundryTrapping[] = []
    if (withWorld) {
      const trappingCategories = CompendiumUtil.getTrappingCategories()
      const worldTrappings = items<FoundryTrapping>()?.filter(
        (item: FoundryTrapping) =>
          trappingCategories.includes(item.type) ||
          trappingCategories.includes(item?.system?.trappingType?.value),
      )
      if (worldTrappings != null && worldTrappings.length > 0) {
        finalTrappings.push(...worldTrappings)
      }
    }
    finalTrappings.push(...trappings)
    return Promise.resolve(finalTrappings)
  }

  public static async getRandomSpeciesCareers(
    speciesKey: string,
    subSpeciesKey?: string | null,
  ): Promise<string[]> {
    if (speciesKey == null) {
      return []
    }
    const concatKey =
      subSpeciesKey != null ? `${speciesKey}-${subSpeciesKey}` : speciesKey
    const humanDefaultKey = 'human-reiklander'
    const tables = await CompendiumUtil.getCompendiumTables()
    const table =
      tables.find(
        (t) =>
          t.flags?.wfrp4e?.key === 'career' &&
          t.flags?.wfrp4e?.column === concatKey,
      ) ??
      tables.find(
        (t) =>
          t.flags?.wfrp4e?.key === 'career' &&
          t.flags?.wfrp4e?.column === speciesKey,
      ) ??
      tables.find(
        (t) =>
          t.flags?.wfrp4e?.key === 'career' &&
          speciesKey === 'human' &&
          t.flags?.wfrp4e?.column === humanDefaultKey,
      )
    const randomCareers: string[] = table?.results?.map((r) => r.text) ?? []

    const careers = await this.getCareerEntities(false)

    const result: string[] = []

    randomCareers.forEach((rdmC) => {
      let rc = rdmC
      if (rc.startsWith('@') && rc.includes('{') && rc.includes('}')) {
        rc = rc.substring(rc.indexOf('{') + 1, rc.lastIndexOf('}'))
      }
      const rcItem = EntityUtil.find(rc, careers)
      if (rcItem) {
        rc = rcItem.name
      }
      let cs = careers.filter((c) =>
        StringUtil.includesDeburrIgnoreCase(c?.system?.careergroup?.value, rc),
      )

      if (cs.length !== 4) {
        const strictCareer = careers.find((c) =>
          StringUtil.equalsDeburrIgnoreCase(c.name ?? '', rc),
        )
        if (strictCareer != null) {
          cs = careers.filter((c) =>
            StringUtil.equalsDeburrIgnoreCase(
              c?.system?.careergroup?.value,
              strictCareer?.system?.careergroup?.value,
            ),
          )
        }
      }

      if (cs.length === 4) {
        result.push(...cs.map((c) => c.name ?? ''))
      }
    })

    return Promise.resolve(result)
  }

  public static getStatusTiers() {
    return wfrp4e().config.statusTiers
  }

  public static getAllBasicSkills(): Promise<FoundrySkill[]> {
    return wfrp4e().utility.allBasicSkills()
  }

  public static async findSkill(name: string): Promise<FoundrySkill> {
    const skills = [
      ...(await this.getWorldEntities<FoundrySkill>('skill')),
      ...(await this.getSkillEntities(false)),
    ]
    const skill = EntityUtil.find<FoundrySkill>(name, skills)
    if (skill == null) {
      throw (
        'Could not find skill (or specialization of) ' +
        name +
        ' in compendum or world'
      )
    }
    return skill
  }

  public static async findTalent(name: string) {
    const talents = [
      ...(await this.getWorldEntities<FoundryTalent>('talent')),
      ...(await this.getTalentEntities(false)),
    ]
    const talent = EntityUtil.find<FoundryTalent>(name, talents)
    if (talent == null) {
      throw (
        'Could not find talent (or specialization of) ' +
        name +
        ' in compendum or world'
      )
    }
    return talent
  }

  public static async findPsychology(name: string) {
    const psychologies = [
      ...(await this.getWorldEntities<FoundryPsychology>('psychology')),
      ...(await this.getPsychologyEntities(false)),
    ]
    const psychology = EntityUtil.find<FoundryPsychology>(name, psychologies)
    if (psychology == null) {
      throw (
        'Could not find psychology (or specialization of) ' +
        name +
        ' in compendum or world'
      )
    }
    if (name.includes('(') && name.includes(')')) {
      const simpleName = StringUtil.getSimpleName(psychology.name ?? name)
      const useName =
        psychology.name?.includes('(') && psychology.name?.includes(')')
          ? psychology.name
          : name
      const groupedName =
        StringUtil.getGroupName(useName) ?? StringUtil.getGroupName(name)
      psychology.name = `${simpleName} (${groupedName})`
    } else {
      if (psychology.name == null) {
        psychology.name = name
      }
      psychology.DisplayName = psychology.name
    }
    return psychology
  }

  public static async findTrait(name: string, onlyRefTrait = false) {
    const traits = [
      ...(await this.getWorldEntities<FoundryTrait>('trait')),
      ...(await this.getTraitEntities(false)),
    ]
    const trait = EntityUtil.find<FoundryTrait>(name, traits)
    if (trait == null) {
      throw (
        'Could not find trait (or specialization of) ' +
        name +
        ' in compendum or world'
      )
    }
    if (onlyRefTrait) {
      return trait
    }

    if (name.includes('(') && name.includes(')')) {
      const simpleName = StringUtil.getSimpleName(trait.name ?? name)
      const useName =
        trait.name?.includes('(') && trait.name?.includes(')')
          ? trait.name
          : name
      const groupedName =
        StringUtil.getGroupName(useName) ?? StringUtil.getGroupName(name)
      trait.name = simpleName
      trait.system.specification.value = groupedName
    } else {
      if (trait.name == null) {
        trait.name = name
      }
      trait.DisplayName = trait.name
    }

    return trait
  }

  public static async findTrappings(
    name: string,
    referentialTrappings?: FoundryTrapping[],
  ): Promise<FoundryTrapping[]> {
    let searchName = StringUtil.toDeburrLowerCase(name)
    const trappings: FoundryTrapping[] = []
    let trapping = await this.findTrapping(searchName, referentialTrappings)
    while (trapping != null) {
      trappings.push(trapping)
      const lastSearch = searchName
      searchName = searchName
        .replace(StringUtil.toDeburrLowerCase(trapping.name.trim()), '')
        .replace('(', '')
        .replace(')', '')
        .trim()
      if (searchName === lastSearch) {
        const simpleName =
          searchName.includes('(') && searchName.includes(')')
            ? searchName.substring(0, searchName.indexOf('(')).trim()
            : searchName
        if (simpleName !== searchName) {
          searchName = searchName
            .replace(StringUtil.toDeburrLowerCase(simpleName), '')
            .replace('(', '')
            .replace(')', '')
            .trim()
        }
      }
      if (searchName === lastSearch) {
        const words = trapping.name
          .trim()
          .split(' ')
          .map((word) => word.trim())
          .filter((word) => word.length > 3)
        for (let word of words) {
          searchName = searchName
            .replace(StringUtil.toDeburrLowerCase(word), '')
            .trim()
        }
      }
      if (searchName.length > 3 && lastSearch !== searchName) {
        trapping = await this.findTrapping(searchName, referentialTrappings)
      } else {
        trapping = null
      }
    }
    if (searchName.trim().includes(' ')) {
      trappings.push(
        ...(await this.findTrappingsByWords(searchName, referentialTrappings)),
      )
    }
    return Promise.resolve(trappings)
  }

  public static async findTrappingsByWords(
    name: string,
    referentialTrappings?: FoundryTrapping[],
  ) {
    const trappings: FoundryTrapping[] = []
    if (name != null && name.length > 0) {
      const words = name
        .split(' ')
        .map((word) => word.trim())
        .map((word) => word.replace('(', '').replace(')', ''))
        .filter((word) => word.length > 3)
      for (let word of words) {
        const trapping = await this.findTrapping(
          word,
          referentialTrappings,
          true,
        )
        if (trapping != null) {
          trappings.push(trapping)
        }
      }
    }
    return trappings
  }

  public static async findTrapping(
    name: string,
    referentialTrappings?: FoundryTrapping[],
    fromWord = false,
  ): Promise<FoundryTrapping | null> {
    const searchTrappings =
      referentialTrappings ?? (await this.getTrappingEntities(true))
    const simpleName =
      name.includes('(') && name.includes(')')
        ? name.substring(0, name.indexOf('(')).trim()
        : name
    let trapping =
      searchTrappings.find((t) =>
        StringUtil.equalsDeburrIgnoreCase(name, t.name ?? ''),
      ) ??
      searchTrappings.find((t) =>
        StringUtil.equalsDeburrIgnoreCase(t.name ?? '', simpleName),
      ) ??
      searchTrappings.find((t) =>
        StringUtil.includesDeburrIgnoreCase(t.name ?? '', name),
      ) ??
      searchTrappings.find((t) =>
        StringUtil.includesDeburrIgnoreCase(t.name ?? '', simpleName),
      )

    if (trapping == null && !fromWord) {
      trapping = searchTrappings
        .sort((t1, t2) => {
          return (t2.name ?? '').length - (t1.name ?? '').length
        })
        .find((t) => StringUtil.includesDeburrIgnoreCase(name, t.name ?? ''))
    }

    if (trapping == null) {
      console.warn(
        `Can't find trapping ${name}${
          simpleName !== name ? `or ${simpleName}` : ''
        }`,
      )
    } else if (!StringUtil.equalsDeburrIgnoreCase(trapping.name ?? '', name)) {
      console.warn(`Trapping ${name} is resolved by ${trapping.name}`)
    }

    return Promise.resolve(trapping ?? null)
  }

  public static async getSpeciesCharacteristics(speciesKey: string) {
    return await wfrp4e().utility.speciesCharacteristics(speciesKey, true)
  }

  public static async getSpeciesMovement(speciesKey: string) {
    return await wfrp4e().utility.speciesMovement(speciesKey)
  }

  public static async getAllMoneyItems(): Promise<FoundryTrapping[]> {
    let moneyItems: FoundryTrapping[] =
      (await wfrp4e().utility.allMoneyItems()) ?? []
    moneyItems = moneyItems
      .map((mi) => {
        mi.system.quantity.value = 0
        return mi
      })
      .sort((a, b) => {
        const aData: FoundryTrappingData = a.system
        const bData: FoundryTrappingData = b.system
        return aData.coinValue.value > bData.coinValue.value ? -1 : 1
      })
    return Promise.resolve(moneyItems)
  }

  public static async getActorsEntities(withWorld = true): Promise<{
    [pack: string]: FoundryActor[]
  }> {
    const actors: {
      [pack: string]: FoundryActor[]
    } = { ...(await CompendiumUtil.getCompendiumActors()) }
    if (withWorld) {
      const worldActors = await this.getWorldActorEntities()
      if (worldActors != null && worldActors.length > 0) {
        actors[world().title] = worldActors
      }
    }
    return Promise.resolve(actors)
  }

  public static async getBestiaryEntities(withWorld = true): Promise<{
    [pack: string]: FoundryActor[]
  }> {
    const bestiary: {
      [pack: string]: FoundryActor[]
    } = { ...(await CompendiumUtil.getCompendiumBestiary()) }
    if (withWorld) {
      const worldActors = await this.getWorldActorEntities('creature')
      if (worldActors != null && worldActors.length > 0) {
        bestiary[world()?.title] = worldActors
      }
    }
    return Promise.resolve(bestiary)
  }

  public static async getSkillEntities(
    withWorld = true,
  ): Promise<FoundrySkill[]> {
    const skills: FoundrySkill[] = [
      ...(await CompendiumUtil.getCompendiumSkills()),
    ]
    if (withWorld) {
      const worldSkills = await this.getWorldEntities<FoundrySkill>('skill')
      if (worldSkills != null && worldSkills.length > 0) {
        skills.push(...worldSkills)
      }
    }
    return Promise.resolve(skills)
  }

  public static async getTalentEntities(
    withWorld = true,
  ): Promise<FoundryTalent[]> {
    const talents: FoundryTalent[] = [
      ...(await CompendiumUtil.getCompendiumTalents()),
    ]
    if (withWorld) {
      const worldTalents = await this.getWorldEntities<FoundryTalent>('talent')
      if (worldTalents != null && worldTalents.length > 0) {
        talents.push(...worldTalents)
      }
    }
    return Promise.resolve(talents)
  }

  public static async getPsychologyEntities(
    withWorld = true,
  ): Promise<FoundryPsychology[]> {
    const psychologies: FoundryPsychology[] = [
      ...(await CompendiumUtil.getCompendiumPsychologies()),
    ]
    if (withWorld) {
      const worldPsychologies =
        await this.getWorldEntities<FoundryPsychology>('psychology')
      if (worldPsychologies != null && worldPsychologies.length > 0) {
        psychologies.push(...worldPsychologies)
      }
    }
    return Promise.resolve(psychologies)
  }

  public static async getTraitEntities(
    withWorld = true,
  ): Promise<FoundryTrait[]> {
    const traits: FoundryTrait[] = [
      ...(await CompendiumUtil.getCompendiumTraits()),
    ]
    if (withWorld) {
      const worldTraits = await this.getWorldEntities<FoundryTrait>('trait')
      if (worldTraits != null && worldTraits.length > 0) {
        traits.push(...worldTraits)
      }
    }
    return Promise.resolve(traits)
  }

  public static async getSpellEntities(
    withWorld = true,
  ): Promise<FoundrySpell[]> {
    const spells: FoundrySpell[] = [
      ...(await CompendiumUtil.getCompendiumSpells()),
    ]
    if (withWorld) {
      const worldSpells = await this.getWorldEntities<FoundrySpell>('spell')
      if (worldSpells != null && worldSpells.length > 0) {
        spells.push(...worldSpells)
      }
    }
    return Promise.resolve(spells)
  }

  public static async getPrayerEntities(
    withWorld = true,
  ): Promise<FoundryPrayer[]> {
    const prayers: FoundryPrayer[] = [
      ...(await CompendiumUtil.getCompendiumPrayers()),
    ]
    if (withWorld) {
      const worldPrayers = await this.getWorldEntities<FoundryPrayer>('prayer')
      if (worldPrayers != null && worldPrayers.length > 0) {
        prayers.push(...worldPrayers)
      }
    }
    return Promise.resolve(prayers)
  }

  public static async getTablesByKey(key: string): Promise<FoundryTable[]> {
    const tables = await CompendiumUtil.getCompendiumTables()
    return tables?.filter((t) => t?.flags?.wfrp4e?.key === key) ?? []
  }

  public static async getMutationMaps(
    refMutationsFct: (withWorld: boolean) => Promise<FoundryMutation[]>,
    refMap: { [key: string]: FoundryMutation[] },
    key: string,
  ): Promise<{
    [key: string]: FoundryMutation[]
  }> {
    if (refMap == null) {
      refMap = {}
      const findedIds: string[] = []
      const tables =
        (await ReferentialUtil.getTablesByKey(key))?.filter(
          (t) => t?.flags?.wfrp4e?.column !== undefined,
        ) ?? []
      const allMutations = [...(await refMutationsFct(true))].sort((t1, t2) => {
        return t1.name.localeCompare(t2.name)
      })
      if (tables.length > 0) {
        const mutations = [...(await refMutationsFct(false))].sort((t1, t2) => {
          return t1.name.localeCompare(t2.name)
        })
        for (const table of tables) {
          refMap[table?.flags?.wfrp4e?.column ?? 'Undivided'] = []
        }
        for (const table of tables) {
          const column = table?.flags?.wfrp4e?.column ?? 'Undivided'
          for (const result of table.results) {
            const id = result.documentId
            const itemData = mutations.find((m) => m._id === id)
            if (itemData != null) {
              const dItemData: FoundryMutation = duplicate(itemData)
              dItemData._id = RandomUtil.getRandomId()
              dItemData.pack = ''
              dItemData.img = ReferentialUtil.mutationsIcons()[column]
              refMap[column].push(dItemData)
              if (!findedIds.includes(itemData._id ?? '')) {
                findedIds.push(itemData._id ?? '')
              }
            }
          }
        }
        const others = allMutations.filter(
          (m) => !findedIds.includes(m._id ?? ''),
        )
        if (others.length > 0) {
          refMap['Undivided'].push(...others)
        }
      } else {
        refMap['Undivided'] = allMutations
      }
    }
    return Promise.resolve(refMap)
  }

  public static async getPhysicalMutationEntities(
    withWorld = true,
  ): Promise<FoundryMutation[]> {
    const physicalMutations: FoundryMutation[] = [
      ...(await CompendiumUtil.getCompendiumPhysicalMutations()),
    ]
    if (withWorld) {
      const worldMutations =
        await ReferentialUtil.getWorldEntities<FoundryMutation>('mutation')
      if (worldMutations != null && worldMutations.length > 0) {
        physicalMutations.push(
          ...worldMutations.filter(
            (m) => m.system.mutationType.value === 'physical',
          ),
        )
      }
    }
    return Promise.resolve(physicalMutations)
  }

  public static async getPhysicalMutationMaps(): Promise<{
    [key: string]: FoundryMutation[]
  }> {
    return ReferentialUtil.getMutationMaps(
      ReferentialUtil.getPhysicalMutationEntities,
      ReferentialUtil.physicalMutationMaps,
      'mutatephys',
    )
  }

  public static async getMentalMutationEntities(
    withWorld = true,
  ): Promise<FoundryMutation[]> {
    const mentalMutations: FoundryMutation[] = [
      ...(await CompendiumUtil.getCompendiumMentalMutations()),
    ]
    if (withWorld) {
      const worldMutations =
        await ReferentialUtil.getWorldEntities<FoundryMutation>('mutation')
      if (worldMutations != null && worldMutations.length > 0) {
        mentalMutations.push(
          ...worldMutations.filter(
            (m) => m?.system?.mutationType.value === 'mental',
          ),
        )
      }
    }
    return Promise.resolve(mentalMutations)
  }

  public static async getMentalMutationMaps(): Promise<{
    [key: string]: FoundryMutation[]
  }> {
    return ReferentialUtil.getMutationMaps(
      ReferentialUtil.getMentalMutationEntities,
      ReferentialUtil.mentalMutationMaps,
      'mutatemental',
    )
  }

  public static async getCompendiumActorTraits() {
    const compendiumActorTraits: FoundryTrait[] = []
    const traits = await CompendiumUtil.getCompendiumTraits()
    const traitsNames = traits.map((t) =>
      EntityUtil.toMinimalName(t.name ?? '').trim(),
    )
    const actorsMap = await this.getActorsEntities()
    for (let [_key, actors] of Object.entries(actorsMap)) {
      for (let actor of actors) {
        const newTraits: FoundryTrait[] =
          actor?.itemTypes?.trait
            ?.map(
              (t: FoundryTrait) => EntityUtil.find(t.name ?? '', traits) ?? t,
            )
            ?.filter(
              (t: FoundryTrait) =>
                !traitsNames.includes(EntityUtil.toMinimalName(t.name).trim()),
            ) ?? []
        if (newTraits != null && newTraits.length > 0) {
          traitsNames.push(
            ...newTraits.map((t) => EntityUtil.toMinimalName(t.name).trim()),
          )
          compendiumActorTraits.push(...newTraits)
        }
      }
    }

    return Promise.resolve(compendiumActorTraits)
  }

  public static async getCompendiumActorSkills() {
    const compendiumActorSkills: FoundrySkill[] = []
    const skills = await CompendiumUtil.getCompendiumSkills()
    const skillsNames = skills.map((t) =>
      EntityUtil.toMinimalName(t.name ?? '').trim(),
    )
    const actorsMap = await this.getActorsEntities()
    for (let [_key, actors] of Object.entries(actorsMap)) {
      for (let actor of actors) {
        const newSkills: FoundrySkill[] =
          actor?.itemTypes?.skill
            ?.map(
              (s: FoundrySkill) => EntityUtil.find(s.name ?? '', skills) ?? s,
            )
            ?.filter(
              (t: FoundrySkill) =>
                !skillsNames.includes(
                  EntityUtil.toMinimalName(t.name).trim(),
                ) && !t.name.trim().startsWith('('),
            ) ?? []
        if (newSkills != null && newSkills.length > 0) {
          skillsNames.push(
            ...newSkills.map((t) => EntityUtil.toMinimalName(t.name).trim()),
          )
          compendiumActorSkills.push(...newSkills)
        }
      }
    }

    return Promise.resolve(compendiumActorSkills)
  }

  public static async getCompendiumActorTalents() {
    const compendiumActorTalents: FoundryTalent[] = []
    const talents = await CompendiumUtil.getCompendiumTalents()
    const talentsNames = talents.map((t) =>
      EntityUtil.toMinimalName(t.name ?? '').trim(),
    )
    const actorsMap = await this.getActorsEntities()
    for (let [_key, actors] of Object.entries(actorsMap)) {
      for (let actor of actors) {
        const newTalents: FoundryTalent[] =
          actor?.itemTypes?.talent
            ?.map(
              (t: FoundryTalent) => EntityUtil.find(t.name ?? '', talents) ?? t,
            )
            ?.filter(
              (t: FoundryTalent) =>
                !talentsNames.includes(
                  EntityUtil.toMinimalName(t.name).trim(),
                ) && !t.name.trim().startsWith('('),
            ) ?? []
        if (newTalents != null && newTalents.length > 0) {
          talentsNames.push(
            ...newTalents.map((t) => EntityUtil.toMinimalName(t.name).trim()),
          )
          compendiumActorTalents.push(...newTalents)
        }
      }
    }

    return Promise.resolve(compendiumActorTalents)
  }

  public static async getSpeciesSkillsMap(): Promise<{
    [key: string]: string[]
  }> {
    if (this.speciesSkillsMap == null) {
      this.speciesSkillsMap = {}
      const coreMap = wfrp4e().config.speciesSkills
      for (let key of Object.keys(coreMap)) {
        const moduleSkills: string[] = speciesReferential.speciesSkills[key]
        if (moduleSkills == null) {
          console.warn(`Cant find fixed species skills map for ${key}`)
        }
        const skills: string[] =
          moduleSkills != null ? moduleSkills : coreMap[key]
        for (let skill of skills) {
          try {
            const refSkill = await ReferentialUtil.findSkill(skill)
            if (refSkill != null) {
              if (this.speciesSkillsMap[key] == null) {
                this.speciesSkillsMap[key] = []
              }
              this.speciesSkillsMap[key].push(refSkill.name)
            }
          } catch (e) {
            console.warn(`Cant find Species ${key} Skill: ${skill}`)
          }
        }
      }
    }
    return Promise.resolve(this.speciesSkillsMap)
  }

  public static async getSpeciesSkills(key: string): Promise<string[]> {
    return Promise.resolve((await this.getSpeciesSkillsMap())[key] ?? [])
  }

  public static async getSubSpeciesSkillsMap(): Promise<{
    [key: string]: {
      [subKey: string]: string[]
    }
  }> {
    if (this.subSpeciesSkillsMap == null) {
      this.subSpeciesSkillsMap = {}
      const coreMap = wfrp4e().config.subspecies
      for (let key of Object.keys(coreMap)) {
        for (let subKey of Object.keys(coreMap[key])) {
          const moduleSkills: string[] =
            speciesReferential.subSpeciesSkills[key] != null
              ? speciesReferential.subSpeciesSkills[key][subKey]
              : null
          if (moduleSkills == null) {
            console.warn(
              `Cant find fixed sub species skills map for ${key} - ${subKey}`,
            )
          }
          const coreSkills =
            coreMap[key][subKey].skills ??
            speciesReferential.speciesSkills[key] ??
            wfrp4e().config.speciesSkills[key]
          const skills: string[] =
            moduleSkills != null ? moduleSkills : coreSkills
          for (let skill of skills) {
            try {
              const refSkill = await ReferentialUtil.findSkill(skill)
              if (refSkill != null) {
                if (this.subSpeciesSkillsMap[key] == null) {
                  this.subSpeciesSkillsMap[key] = {}
                }
                if (this.subSpeciesSkillsMap[key][subKey] == null) {
                  this.subSpeciesSkillsMap[key][subKey] = []
                }
                this.subSpeciesSkillsMap[key][subKey].push(refSkill.name)
              }
            } catch (e) {
              console.warn(
                `Cant find sub Species ${key} - ${subKey} Skill: ${skill}`,
              )
            }
          }
        }
      }
    }
    return Promise.resolve(this.subSpeciesSkillsMap)
  }

  public static async getSubSpeciesSkills(
    key: string,
    subKey: string,
  ): Promise<string[]> {
    return Promise.resolve(
      (await this.getSubSpeciesSkillsMap())[key][subKey] ?? [],
    )
  }

  public static async getSpeciesOthersMap(): Promise<{
    [key: string]: (string | number)[]
  }> {
    if (this.speciesOthersMap == null) {
      this.speciesOthersMap = {}
      await this.initSpeciesEntities(
        this.speciesOthersMap,
        wfrp4e().config.speciesTalents,
        speciesReferential.speciesOthers,
        'Talent',
      )
    }
    return Promise.resolve(this.speciesOthersMap)
  }

  public static async getSpeciesOthers(
    key: string,
    onlyToChoose = false,
    onlyAuto = false,
  ): Promise<(string | number)[]> {
    return Promise.resolve(
      (await this.getSpeciesOthersMap())[key]?.filter(
        this.speciesRefFilter(onlyToChoose, onlyAuto),
      ) ?? [],
    )
  }

  public static async getSubSpeciesOthersMap(): Promise<{
    [key: string]: {
      [subKey: string]: (string | number)[]
    }
  }> {
    if (this.subSpeciesOthersMap == null) {
      this.subSpeciesOthersMap = {}

      for (let key of Object.keys(this.getSpeciesMap())) {
        if (wfrp4e().config.subspecies[key] != null) {
          const coreMap: { [key: string]: (string | number)[] } = {}
          for (let subKey of Object.keys(wfrp4e().config.subspecies[key])) {
            if (wfrp4e().config.subspecies[key][subKey].talents != null) {
              coreMap[subKey] = wfrp4e().config.subspecies[key][subKey]
                .talents as (string | number)[]
            } else {
              coreMap[subKey] = await this.getSpeciesOthers(key)
            }
          }
          const currentSubSpeciesMap: { [key: string]: (string | number)[] } =
            {}
          await this.initSpeciesEntities(
            currentSubSpeciesMap,
            coreMap,
            speciesReferential.subSpeciesOthers[key],
            'Talent',
          )
          this.subSpeciesOthersMap[key] = currentSubSpeciesMap
        }
      }
    }
    return Promise.resolve(this.subSpeciesOthersMap)
  }

  public static async getSubSpeciesOthers(
    key: string,
    subKey: string,
    onlyToChoose = false,
    onlyAuto = false,
  ): Promise<(string | number)[]> {
    return Promise.resolve(
      (await this.getSubSpeciesOthersMap())[key][subKey]?.filter(
        this.speciesRefFilter(onlyToChoose, onlyAuto),
      ) ?? [],
    )
  }

  public static async getSpeciesOriginsMap(): Promise<{
    [key: string]: {
      [origin: string]: (string | number)[]
    }
  }> {
    if (this.speciesOriginsMap == null) {
      this.speciesOriginsMap = {}
      for (let key of Object.keys(this.getSpeciesMap())) {
        if (speciesReferential.speciesOrigin[key] != null) {
          const currentOriginsMap: { [key: string]: (string | number)[] } = {}
          await this.initSpeciesEntities(
            currentOriginsMap,
            speciesReferential.speciesOrigin[key],
            speciesReferential.speciesOrigin[key],
            'Origin',
          )
          this.speciesOriginsMap[key] = currentOriginsMap
        }
      }
    }
    return Promise.resolve(this.speciesOriginsMap)
  }

  public static async getSpeciesOrigins(
    key: string,
    onlyToChoose = false,
    onlyAuto = false,
  ): Promise<{ [origin: string]: (string | number)[] }> {
    const map = (await this.getSpeciesOriginsMap())[key] ?? {}
    const result = {}
    for (let origin of Object.keys(map)) {
      result[origin] =
        map[origin]?.filter(this.speciesRefFilter(onlyToChoose, onlyAuto)) ?? []
    }
    return Promise.resolve(result)
  }

  public static async getSpeciesOrigin(
    key: string,
    origin: string,
    onlyToChoose = false,
    onlyAuto = false,
  ): Promise<(string | number)[]> {
    return Promise.resolve(
      (await this.getSpeciesOriginsMap())[key][origin]?.filter(
        this.speciesRefFilter(onlyToChoose, onlyAuto),
      ) ?? [],
    )
  }

  public static async getSubSpeciesOriginsMap(): Promise<{
    [key: string]: {
      [subKey: string]: {
        [origin: string]: (string | number)[]
      }
    }
  }> {
    if (this.subSpeciesOriginsMap == null) {
      this.subSpeciesOriginsMap = {}
      for (let key of Object.keys(this.getSpeciesMap())) {
        if (speciesReferential.subSpeciesOrigins[key] != null) {
          for (let subKey of Object.keys(wfrp4e().config.subspecies[key])) {
            if (speciesReferential.subSpeciesOrigins[key][subKey] != null) {
              const currentOriginsMap: { [key: string]: (string | number)[] } =
                {}
              await this.initSpeciesEntities(
                currentOriginsMap,
                speciesReferential.subSpeciesOrigins[key][subKey],
                speciesReferential.subSpeciesOrigins[key][subKey],
                'Origin',
              )
              if (this.subSpeciesOriginsMap[key] == null) {
                this.subSpeciesOriginsMap[key] = {}
              }
              this.subSpeciesOriginsMap[key][subKey] = currentOriginsMap
            }
          }
        } else {
          this.subSpeciesOriginsMap[key] = {}
        }
      }
    }
    return Promise.resolve(this.subSpeciesOriginsMap)
  }

  public static async getSubSpeciesOrigins(
    key: string,
    subKey: string,
    onlyToChoose = false,
    onlyAuto = false,
  ): Promise<{ [origin: string]: (string | number)[] }> {
    const originMap = await this.getSubSpeciesOriginsMap()
    const map = (originMap[key] != null ? originMap[key][subKey] : {}) ?? {}
    const result = {}
    for (let origin of Object.keys(map)) {
      result[origin] =
        map[origin]?.filter(this.speciesRefFilter(onlyToChoose, onlyAuto)) ?? []
    }
    return Promise.resolve(result)
  }

  public static async getSubSpeciesOrigin(
    key: string,
    subKey: string,
    origin: string,
    onlyToChoose = false,
    onlyAuto = false,
  ): Promise<(string | number)[]> {
    return Promise.resolve(
      (await this.getSubSpeciesOriginsMap())[key][subKey][origin]?.filter(
        this.speciesRefFilter(onlyToChoose, onlyAuto),
      ) ?? [],
    )
  }

  public static async getRandomTalents(): Promise<string[]> {
    if (this.randomTalents == null) {
      this.randomTalents = []
      for (let talent of speciesReferential.randomTalents) {
        try {
          const refTalent = await this.findTalent(talent)
          this.randomTalents.push(refTalent.name)
        } catch (e) {
          console.warn(`Cant find Random Talent : ${talent}`)
        }
      }
    }
    return Promise.resolve(this.randomTalents)
  }

  private static async initSpeciesEntities(
    targetMap: { [key: string]: (string | number)[] },
    coreMap: { [key: string]: (string | number)[] },
    moduleMap: { [key: string]: (string | number)[] },
    logType: string,
    onlyModule = false,
  ) {
    for (let key of Object.keys(coreMap)) {
      const moduleEntities: (string | number)[] = moduleMap[key]
      if (moduleEntities == null) {
        console.warn(`Cant find fixed sub species ${logType} map for ${key}`)
        if (onlyModule) {
          continue
        }
      }
      const entities: (string | number)[] =
        moduleEntities != null ? moduleEntities : coreMap[key]
      for (let entity of entities) {
        const isNumber = typeof entity === 'number'
        const search = isNumber ? null : this.resolveSearch(entity)
        const finalEntity = isNumber ? entity : this.resolveName(entity)
        const prefix = isNumber ? '' : this.resolvePrefix(entity)
        try {
          if (isNumber) {
            if (targetMap[key] == null) {
              targetMap[key] = []
            }
            targetMap[key].push(finalEntity)
          } else if ((finalEntity as string).includes(',')) {
            const multiEntities = (finalEntity as string)
              .split(',')
              .map((t) => t.trim())
            let finalMultiEntity = ''
            for (let multiEntity of multiEntities) {
              const multiSearch = this.resolveSearch(multiEntity)
              const multiFinalEntity = this.resolveName(multiEntity)
              const multiPrefix = this.resolvePrefix(multiEntity)
              const refEntity =
                multiSearch != null ? await multiSearch(multiFinalEntity) : null
              if (refEntity != null) {
                if (finalMultiEntity.length > 0) {
                  finalMultiEntity += ', '
                }
                finalMultiEntity += `${multiPrefix}${refEntity.name}`
              }
              if (refEntity == null && multiPrefix.length > 0) {
                if (finalMultiEntity.length > 0) {
                  finalMultiEntity += ', '
                }
                finalMultiEntity += `${multiEntity}`
              }
            }
            if (finalMultiEntity.length > 0) {
              if (targetMap[key] == null) {
                targetMap[key] = []
              }
              targetMap[key].push(finalMultiEntity)
            }
          } else if (search == null && prefix.length > 0) {
            targetMap[key].push(entity)
          } else {
            const refTalent =
              search != null ? await search(finalEntity as string) : null
            if (refTalent != null) {
              if (targetMap[key] == null) {
                targetMap[key] = []
              }
              targetMap[key].push(`${prefix}${refTalent.name}`)
            }
          }
        } catch (e) {
          console.warn(`Cant find Species ${key} ${logType}: ${entity}`)
        }
      }
    }
  }

  public static resolveName(name: string): string {
    if (name?.startsWith(traitPrefix)) {
      return name.replace(traitPrefix, '')
    } else if (name?.startsWith(psychologyPrefix)) {
      return name.replace(psychologyPrefix, '')
    }
    return name
  }

  public static resolveRandomPrefix(name: string): number {
    if (!name.startsWith(randomTalent)) {
      return 0
    }
    return Number(name.replace(randomTalent, ''))
  }

  private static resolvePrefix(name: string): string {
    if (name?.startsWith(traitPrefix)) {
      return traitPrefix
    } else if (name?.startsWith(psychologyPrefix)) {
      return psychologyPrefix
    } else if (name?.startsWith(randomTalent)) {
      return randomTalent
    } else if (name?.startsWith(origin)) {
      return origin
    }
    return ''
  }

  private static resolveSearch(
    name: string,
  ): ((searchValue: string) => Promise<FoundryItem>) | null {
    if (name?.startsWith(traitPrefix)) {
      return (searchValue) => this.findTrait(searchValue, true)
    } else if (name?.startsWith(psychologyPrefix)) {
      return (searchValue) => this.findPsychology(searchValue)
    } else if (name?.startsWith(randomTalent) || name?.startsWith(origin)) {
      return null
    }
    return (searchValue) => this.findTalent(searchValue)
  }

  private static speciesRefFilter(onlyToChoose: boolean, onlyAuto: boolean) {
    return (sso) => {
      const strSoo = String(sso)
      const toChooseCondition =
        typeof sso === 'number' || strSoo.includes(',') || sso === origin
      if (onlyToChoose) {
        return toChooseCondition
      } else if (onlyAuto) {
        return !toChooseCondition
      }
      return true
    }
  }

  public static getNativeTongue(
    speciesKey: string,
    subSpeciesKey: string,
  ): string | null {
    const speciesNatTongue = speciesReferential.speciesNativeTongue[speciesKey]
    const subSpeciesNatTongue =
      subSpeciesKey?.length > 0 &&
      speciesReferential.subSpeciesNativeTongue[speciesKey] != null
        ? speciesReferential.subSpeciesNativeTongue[speciesKey][subSpeciesKey]
        : null
    return subSpeciesNatTongue != null ? subSpeciesNatTongue : speciesNatTongue
  }
}
