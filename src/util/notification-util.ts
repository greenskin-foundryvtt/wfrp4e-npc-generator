import { notifications } from '../constant'

export default class NotificationUtil {
  private static _silent = false

  public static get silent(): boolean {
    return this._silent
  }

  public static set silent(value: boolean) {
    const notifContainer = document.getElementById(notifications().id)
    if (notifContainer) {
      if (value) {
        notifContainer.classList.add('wfrp4e-npc-generator-notifications-hide')
      } else {
        notifications().clear()
        setTimeout(() => {
          notifContainer.classList.remove(
            'wfrp4e-npc-generator-notifications-hide',
          )
        }, 5000)
      }
    }
    this._silent = value
  }
}
