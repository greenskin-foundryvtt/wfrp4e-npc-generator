import { SourceSelectModel } from './source-select-model'

export class GroupedSourceSelectModel {
  constructor(group: string) {
    this.group = group
  }

  public group: string
  public items: SourceSelectModel[] = []
}
