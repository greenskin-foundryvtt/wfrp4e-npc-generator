export declare type CharsModel = { [char: string]: CharModel }

export class CharModel {
  constructor(initial: number, advances: number) {
    this.initial = initial
    this.advances = advances
  }

  public initial: number
  public advances: number
  public value?: number
}
