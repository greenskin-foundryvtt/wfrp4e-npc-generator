import EntityUtil from '../../util/entity-util'
import { FoundryItem } from '../../types/foundry/item'

export class SourcedModel {
  constructor(source: FoundryItem, name: string) {
    this.source = source
    this.name = name
    this.id = source?._id ?? ''
    this.isWorldItem = EntityUtil.isWorldItem(source)
    this.img = source?.img ?? 'systems/wfrp4e/icons/blank.png'
  }

  public source: FoundryItem
  public id: string
  public name: string
  public isWorldItem: boolean
  public img: string
}
