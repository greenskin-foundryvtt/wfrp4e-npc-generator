import { ActorModel } from './actor-model'
import { CharsModel } from './char-model'
import { FoundryCareer } from '../../types/foundry/career'
import { FoundrySkill } from '../../types/foundry/skill'
import { FoundryTalent } from '../../types/foundry/talent'
import { FoundryTrait } from '../../types/foundry/trait'
import { FoundryPsychology } from '../../types/foundry/psychology'
import { FoundrySpell } from '../../types/foundry/spell'
import { FoundryPrayer } from '../../types/foundry/prayer'
import { FoundryMutation } from '../../types/foundry/mutation'
import { FoundryTrapping } from '../../types/foundry/trapping'

export abstract class ActorModelBuilder<T extends ActorModel> {
  constructor(model: T) {
    this.model = model
  }

  public model: T

  public skills: FoundrySkill[] = []
  public talents: FoundryTalent[] = []
  public traits: FoundryTrait[] = []
  public psychologies: FoundryPsychology[] = []
  public spells: FoundrySpell[] = []
  public prayers: FoundryPrayer[] = []
  public physicalMutations: FoundryMutation[] = []
  public mentalMutations: FoundryMutation[] = []

  public chars: CharsModel = {}

  public move: string

  public trappings: FoundryTrapping[] = []

  public speciesValue: string

  public status: string

  public careers: FoundryCareer[] = []
  public career: FoundryCareer
  public careerForSkills: FoundryCareer[] = []
}
