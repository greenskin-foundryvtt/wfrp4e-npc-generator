import { SourcedModel } from './sourced-model'
import { FoundryItem } from '../../types/foundry/item'

export class TrappingModel extends SourcedModel {
  constructor(source: FoundryItem, name: string) {
    super(source, name)
    this.quantity = Number(source?.system?.quantity?.value ?? 0)
  }

  public quantity: number
}
