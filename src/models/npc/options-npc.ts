import { IOptions } from '../common/options-int'
import { settings } from '../../constant'
import RegisterSettings from '../../util/register-settings'
import {
  GenerateEffectOptionEnum,
  getGenerateEffectOptionEnum,
} from '../../util/generate-effect-option.enum'

export class OptionsNpc implements IOptions {
  public withClassTrappings: boolean = settings().get(
    RegisterSettings.moduleName,
    'defaultWithClassTrappings',
  )
  public withCareerTrappings: boolean = settings().get(
    RegisterSettings.moduleName,
    'defaultWithCareerTrappings',
  )
  public generateMoneyEffect: GenerateEffectOptionEnum =
    getGenerateEffectOptionEnum(
      settings().get(RegisterSettings.moduleName, 'defaultGenerateMoneyEffect'),
    )
  public generateWeaponEffect: GenerateEffectOptionEnum =
    getGenerateEffectOptionEnum(
      settings().get(
        RegisterSettings.moduleName,
        'defaultGenerateWeaponEffect',
      ),
    )

  public withGenPathCareerName = settings().get<boolean>(
    RegisterSettings.moduleName,
    'defaultWithGenPathCareerName',
  )

  public withLinkedToken = settings().get<boolean>(
    RegisterSettings.moduleName,
    'defaultWithLinkedToken',
  )

  public withInitialMoney = settings().get<boolean>(
    RegisterSettings.moduleName,
    'defaultWithInitialMoney',
  )

  public withInitialWeapons = settings().get<boolean>(
    RegisterSettings.moduleName,
    'defaultWithInitialWeapons',
  )

  public genPath: string = settings().get(
    RegisterSettings.moduleName,
    'defaultGenPath',
  )

  public imagePath: string | null = null

  public tokenPath: string | null = null

  public editAbilities = settings().get<boolean>(
    RegisterSettings.moduleName,
    'defaultEditAbilities',
  )

  public editTrappings = settings().get<boolean>(
    RegisterSettings.moduleName,
    'defaultEditTrappings',
  )

  public addMagics = settings().get<boolean>(
    RegisterSettings.moduleName,
    'defaultAddMagics',
  )

  public addMutations = settings().get<boolean>(
    RegisterSettings.moduleName,
    'defaultAddMutations',
  )

  public noRandomCaras = settings().get<boolean>(
    RegisterSettings.moduleName,
    'defaultNoRandomCarac',
  )

  public noSkillAdvances = settings().get<boolean>(
    RegisterSettings.moduleName,
    'defaultNoSkillAdvances',
  )
}
