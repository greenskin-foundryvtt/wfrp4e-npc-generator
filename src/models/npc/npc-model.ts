import { ActorModel } from '../common/actor-model'
import { SpeciesSkills } from './species-skills'
import { SpeciesOthers } from './species-others'
import { OptionsNpc } from './options-npc'
import { NpcModelBuilder } from './npc-model-builder'

export class NpcModel extends ActorModel {
  constructor() {
    super()
    this.options = new OptionsNpc()
  }
  public speciesKey: string
  public subSpeciesKey: string
  public speciesSkills: SpeciesSkills = new SpeciesSkills()
  public speciesOthers: SpeciesOthers = new SpeciesOthers()
  public fromBuilder: NpcModelBuilder | undefined = undefined
  public fromStanding: number
  public fromTier: string
}
