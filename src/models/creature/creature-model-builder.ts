import { ActorModelBuilder } from '../common/actor-model-builder'
import { CreatureModel } from './creature-model'
import { FoundryItem } from '../../types/foundry/item'

export class CreatureModelBuilder extends ActorModelBuilder<CreatureModel> {
  constructor(model: CreatureModel) {
    super(model)
    this.skills = [...model.abilities.skills]
    this.talents = [...model.abilities.talents]
    this.traits = [...model.abilities.traits]
    this.trappings = [...model.trappings]
    this.spells = [...model.spells]
    this.prayers = [...model.prayers]
    this.physicalMutations = [...model.physicalMutations]
    this.mentalMutations = [...model.mentalMutations]
    this.others = [...model.others]
    this.sizeKey = String(model.abilities.sizeKey)
    this.move = model.creature.system.details.move?.value ?? ''
    this.speciesValue = model.creature.system.details.species?.value ?? ''
    this.biography = model.creature.system.details.biography?.value ?? ''
    this.gender = model.creature.system.details.gender?.value ?? ''
    this.hitLocationTable =
      model.creature.system.details.hitLocationTable?.value ?? ''
    this.status = model.creature.system.details.status?.value ?? ''
  }

  public sizeKey: string = 'avg'
  public biography: string
  public gender: string
  public hitLocationTable: string

  public others: FoundryItem[] = []
}
